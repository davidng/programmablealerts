<%@ include file="/WEB-INF/template/include.jsp" %>

<openmrs:require privilege="View Programmable Alerts Properties" otherwise="/login.htm" redirect="/module/programmablealerts/alertErrors.list"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>

<c:set var="alertErrorSize" value="${fn:length(alertErrors)}" />
<div>
	<b class="boxHeader">
		<spring:message code="programmablealerts.alertErrors.header"/>
	</b>
	<c:choose>
		<c:when test="${alertErrorSize < 1}">
			<br/>
			<i>(<spring:message code="list.empty"/>)</i>
			<br/>
		</c:when>
		<c:otherwise>
			<div class="box">
				<form method="post" name="errorForm" onSubmit="return checkSelected(this)">
					<table cellpadding="8" cellspacing="0" colspan="2">
						<tr>
							<th></th>
							<th><spring:message code="general.view"/>/<spring:message code="general.edit"/></th>
							<th></th>
							<th><spring:message code="general.name"/></th>
							<th><spring:message code="Patient.title"/></th>
							<th><spring:message code="programmablealerts.lastAttempt"/></th>
						</tr>
						<c:forEach items="${alertErrors}" var="alert">
							<tr>
								<td>
									<input type="checkbox" name="errorId" value="${alert.id}" onclick="clearError('errorId')"/>
								</td>
								<td align="center">
									<a href="javascript:void(0)" onClick="loadUrlIntoMessagePopup('<spring:message code="programmablealerts.alertDetails"/>', '${pageContext.request.contextPath}/module/programmablealerts/alertErrorDisplay.form?alertId=${alert.id}'); return false;">
										<img src="${pageContext.request.contextPath}/images/edit.gif" title="View" border="0" align="top" /></a>
								</td>
								<td></td>
								<td>${alert.alertName}</td>
								<td><a href="${pageContext.request.contextPath}/patientDashboard.form?patientId=${alert.patient.id}">${alert.patient.personName.fullName}</a></td>
								<td><openmrs:formatDate date="${alert.lastAttempt}" type="long" /></td>
							</tr>
							
						</c:forEach>
					</table>
					<input type="submit" name="delete" value="<spring:message code="programmablealerts.delete.errors"/>" onclick="return confirm('<spring:message code="programmablealerts.confirmDelete"/>');" />
					<span class="error" id="errorIdError"><spring:message code="programmablealerts.nothingSelected"/></span>
				</form>
				<input type="hidden" value=${page} name="page"/>
				<table width="100%" cellpadding="8">
					<tr>
						<td align="center">
							<c:choose>
								<c:when test="${page > 1}">
									<a href="javascript:void(0)" onClick="window.parent.location = '${pageContext.request.contextPath}/module/programmablealerts/alertErrorList.list?page=${page - 2}'; return false;"><spring:message code="programmablealerts.previous"/></a>
								</c:when>
								<c:otherwise>
									<spring:message code="programmablealerts.previous"/>
								</c:otherwise>
							</c:choose> |
							<c:choose>
								<c:when test="${!endPage}">
									<a href="javascript:void(0)" onClick="window.parent.location = '${pageContext.request.contextPath}/module/programmablealerts/alertErrorList.list?page=${page}'; return false;"><spring:message code="programmablealerts.next"/></a>
								</c:when>
								<c:otherwise>
									<spring:message code="programmablealerts.next"/>
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</table>
			</div>
		</c:otherwise>
	</c:choose>
</div>

<script src="${pageContext.request.contextPath}/scripts/jquery/jquery-1.3.2.min.js" type="text/javascript" ></script>
<link href="${pageContext.request.contextPath}/scripts/jquery/dataTables/css/dataTables.css" type="text/css" rel="stylesheet" />
<script src="${pageContext.request.contextPath}/scripts/jquery/dataTables/js/jquery.dataTables.min.js" type="text/javascript" ></script>
<script src="${pageContext.request.contextPath}/scripts/jquery-ui/js/jquery-ui-1.7.2.custom.min.js" type="text/javascript" ></script>
<link href="${pageContext.request.contextPath}/scripts/jquery-ui/css/redmond/jquery-ui-1.7.2.custom.css" type="text/css" rel="stylesheet" />

<script type="text/javascript">
	var $j = jQuery.noConflict(); 
</script>

<div id="displayMessagePopup">
	<iframe id="displayMessagePopupIframe" width="100%" height="100%" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="auto"></iframe>
</div>

<script type="text/javascript">
	$j(document).ready(function() {
		$j('#displayMessagePopup').dialog({
				title: 'dynamic',
				autoOpen: false,
				draggable: false,
				resizable: false,
				width: '70%',
				modal: true
		});
	});

	function loadUrlIntoMessagePopup(title, urlToLoad) {
		$j("#displayMessagePopupIframe").attr("src", urlToLoad);
		$j('#displayMessagePopup')
			.dialog('option', 'title', title)
			.dialog('option', 'height', $j(window).height() - 50) 
			.dialog('open');
	}

</script>
<script type="text/javascript">
	clearError("errorId");

	function checkSelected(form) {
		
	    //get total number of CheckBoxes in form
	    var formLength = form.length;
	    var chkBoxCount = 0;
	    for (i=0;i<formLength;i++){
			if (form[i].type == 'checkbox') 
				chkBoxCount++;
		}

	    if (chkBoxCount==1) { //we dont have an array
	    	if (form.errorId.checked) 
	        {
	            //it's checked so return true and exit
	            return true;
	        }
	    }else {
		    //loop through each CheckBox
		    for (var i = 0; i < chkBoxCount; i++) 
		    {
		        if (form.errorId[i].checked) 
		        {
		            //it's checked so return true and exit
		            return true;
		        }
		    }
	    }
	    document.getElementById("errorIdError").style.display = "";
	    return false;
	}

	function clearError(errorName) {
		document.getElementById(errorName + "Error").style.display = "none";
	}
</script>


<br/><br/>
<%@ include file="/WEB-INF/template/footer.jsp" %>