<%@ include file="/WEB-INF/template/include.jsp" %>

<openmrs:require privilege="View Programmable Alerts Properties" otherwise="/login.htm" redirect="/module/programmablealerts/alertTemplate.form"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>

<h2><spring:message code="programmablealerts.createNewAlertTemplate"/></h2>
<form method="post" onSubmit="return validateForm()">
	<div>
		<b class="boxHeader">
			<spring:message code="programmablealerts.newAlertTemplate"/>
		</b>
		<div class="box">
			<input type="hidden" name="alertTemplateId" value="${alertTemplate.templateId}"/>
			<table>
				<tr>
					<td><spring:message code="general.name"/></td>
					<td>
						<input type="text" id="alertTemplateName" name="alertTemplateName" size="40" 
							value="${alertTemplate.templateName}" onKeyUp="clearError('alertTemplateName')"/>
						<span class="error" id="alertTemplateNameError">
							<spring:message code="programmablealerts.requiredField"/></span>
					</td>
				</tr>
				<tr>
					<td><spring:message code="feedback.subject"/></td>
					<td>
						<input type="text" id="subject" name="subject" size="40" value="${alertTemplate.subject}" onKeyUp="clearError('subject')"/>
						<span class="error" id="subjectError">
							<spring:message code="programmablealerts.requiredField"/>
						</span>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<spring:message code="programmablealerts.emailBody"/>
					</td>
					<td>
						<textarea id="emailBody" name="emailBody" rows="10" cols="50" type="_moz" onKeyUp="clearError('emailBody')">${alertTemplate.message}</textarea>
						<span class="error" id="emailBodyError">
							<spring:message code="programmablealerts.requiredField"/>
						</span>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<spring:message code="programmablealerts.insertObject"/>:
						<select id="objectValue" name="objectValue">
							<option value="Person Name">Person Name</option>
							<option value="Person Identifier">Person Identifier</option>
							<option value="Health Center Name">Health Center Name</option>
							<option value="Patient Id">Patient Id</option>
							<option value="Base URL">Base URL</option>
							<c:forEach items="${props}" var="prop">
								<option value="${prop.description}">${prop.description}</option>
							</c:forEach>
							<c:forEach items="${ptypes}" var="type">
								<option value="${type}">${type}</option>
							</c:forEach>
						</select>
						<input type="button" onclick="insertObject(); return false;" value="<spring:message code="programmablealerts.insert"/>"/>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<br/>
	<a href="alertTemplateList.list"><input type="button" value="<spring:message code="programmablealerts.discardChanges"/>"/></a>
	<input type="submit" value="<spring:message code="programmablealerts.saveTemplate"/>"/>
</form>

<script type="text/javascript">
	clearError("alertTemplateName");
	clearError("subject");
	clearError("emailBody");
		
	function validateForm() {
		var alertTemplateName = document.getElementById("alertTemplateName");
		var subject = document.getElementById("subject");
		var emailBody = document.getElementById("emailBody");
		
		var result = true;
		if (alertTemplateName.value == "") {
			document.getElementById("alertTemplateNameError").style.display = "";
			result = false;
		}
		
		if (subject.value == "") {
			document.getElementById("subjectError").style.display = "";
			result = false;
		}
		
		if (emailBody.value == "") {
			document.getElementById("emailBodyError").style.display = "";
			result = false;
		}
		
		return result;
	}

	function clearError(errorName) {
		document.getElementById(errorName + "Error").style.display = "none";
	}
	
	function insertObject(objectValue) {
		var emailBody = document.getElementById("emailBody");
		var objectValue = document.getElementById("objectValue").value;
		objectValue='{' + objectValue + '}';
		
		//IE support
		if (document.selection) {
			emailBody.focus();
			sel = document.selection.createRange();
			sel.text = objectValue;
		}
		
		//MOZILLA/NETSCAPE support
		else if (emailBody.selectionStart || emailBody.selectionStart == '0') {
			var startPos = emailBody.selectionStart;
			var endPos = emailBody.selectionEnd;
			emailBody.value = emailBody.value.substring(0, startPos)
			+ objectValue
			+ emailBody.value.substring(endPos, emailBody.value.length);
		} else {
			emailBody.value += objectValue;
		}
	}
</script>
<%@ include file="/WEB-INF/template/footer.jsp" %>