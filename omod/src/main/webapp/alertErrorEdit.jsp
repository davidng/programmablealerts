<%@ include file="/WEB-INF/template/include.jsp" %>

<openmrs:require privilege="View Programmable Alerts Properties" otherwise="/login.htm" redirect="/module/programmablealerts/errorEdit.form"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>
<openmrs:htmlInclude file="/scripts/dojo/dojo.js" />

<script type="text/javascript">
	dojo.require("dojo.widget.openmrs.UserSearch");
	dojo.require("dojo.widget.openmrs.OpenmrsPopup");
	
	dojo.addOnLoad( function() {
		
		searchWidget = dojo.widget.manager.getWidgetById("uSearch");			
		
		dojo.event.topic.subscribe("uSearch/select", 
			function(msg) {
				for (var i=0; i< msg.objs.length; i++) {
					var obj = msg.objs[i];
					var options = $("userNames").options;
					var isAddable = true;
					for (x=0; x<options.length; x++)
						if (options[x].value == obj.userId)
							isAddable = false;
					
					if (isAddable) {
						var opt = new Option(obj.personName, obj.userId);
						opt.selected = true;
						options[options.length] = opt;
						copyIds("userNames", "userIds", ",");
					}
				}
			}
		);
	});
	
</script>

<script type="text/javascript">
		
	function removeItem(nameList, idList, delim)
	{
		var sel   = document.getElementById(nameList);
		var input = document.getElementById(idList);
		var optList   = sel.options;
		var lastIndex = -1;
		var i = 0;
		while (i<optList.length) {
			// loop over and erase all selected items
			if (optList[i].selected) {
				optList[i] = null;
				lastIndex = i;
			}
			else {
				i++;
			}
		}
		copyIds(nameList, idList, delim);
		while (lastIndex >= optList.length)
			lastIndex = lastIndex - 1;
		if (lastIndex >= 0) {
			optList[lastIndex].selected = true;
			return optList[lastIndex];
		}
		return null;
	}
	
	function copyIds(from, to, delimiter)
	{
		var sel = document.getElementById(from);
		var input = document.getElementById(to);
		var optList = sel.options;
		var remaining = new Array();
		var i=0;
		while (i < optList.length)
		{
			remaining.push(optList[i].value);
			i++;
		}
		input.value = remaining.join(delimiter);
		
		if (optList.length > 0){
			clearError("recipients");
		}
	}
	
	function listKeyPress(from, to, delim, event) {
		var keyCode = event.keyCode;
		if (keyCode == 8 || keyCode == 46) {
			removeItem(from, to, delim);
			//attempt to prevent backspace key (#8) from going back in browser
			window.Event.keyCode = 0;
		}
	}
</script>

<style type="text/css">
	.duplicateButton {
		font-size: .8em;
		border: 1px solid darkgrey;
		background-color: whitesmoke;
		cursor: pointer;
		margin: 1px;
		padding: 1px;
	}.selectWidth {
		width: 295px;
		}
		select.selectWidth {
			width: 295px;
		}
</style>
<div>
	<b class="boxHeader"><spring:message code="programmablealerts.error.correct"/></b>
	<form method="post" onSubmit="return validateForm()">
		<div class="box">
			<input type="hidden" name="alertId" value="${alert.id}"/>
			<table>
				<tr>
					<td>
						<spring:message code="general.name"/>
					</td>
					<td>
						<input type="text" style="background-color: whitesmoke" readonly="true" name="alertName" size="40" value="${alert.alertName}"/>
					</td>
				</tr>
				<tr>
					<td valign="top"><spring:message code="Alert.recipients"/></td>
					<td valign="top">
						<input type="hidden" name="userIds" id="userIds" size="40" value='<c:forEach items="${recipients}" var="recipient">${recipient.id} </c:forEach>' />
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top">
									<select class="selectWidth" size="6" id="userNames" multiple onkeyup="listKeyPress('userNames', 'userIds', ' ', event);">
										<c:forEach items="${recipients}" var="recipient">
											<option value="${recipient.id}">${recipient.personName}</option>
										</c:forEach>
									</select>
								</td>
								<td valign="top" class="buttons">
									&nbsp;<span dojoType="UserSearch" widgetId="uSearch"></span><span dojoType="OpenmrsPopup" searchWidget="uSearch" searchTitle='<spring:message code="programmablealerts.recipients.search"/>' changeButtonValue='<spring:message code="general.add"/>'></span>
									&nbsp; <input type="button" value="<spring:message code="general.remove"/>" class="smallButton" onClick="removeItem('userNames', 'userIds', ',');" /> <br/><br/>
									&nbsp;<span class="error" id="recipientsError">
												<spring:message code="programmablealerts.requiredField"/></span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><spring:message code="feedback.subject"/></td>
					<td><input type="text" style="background-color: whitesmoke" id="subject" readonly="true" name="subject" size="40" value="${alert.subject}"/></td>
				</tr>
				<tr>
					<td valign="top"><spring:message code="programmablealerts.emailBody"/></td>
					<td><textarea style="background-color: whitesmoke" id="emailbody" name="emailbody" readonly="true" rows="5" cols="52">${alert.message}</textarea></td>
				</tr>
				<tr>
					<td><spring:message code="programmablealerts.savedSearch"/>:</td>
					<input type="hidden" name="savedSearch" value="${alert.alertTrigger.savedSearch}"/></td>
					<td><input type="text" style="background-color: whitesmoke" readonly="true" size="40" value="${alert.alertTrigger.savedSearch}"/></td>
				</tr>
			</table>
		</div>
		<br/>
		<a href="alertErrorList.list"><input type="button" value="<spring:message code="programmablealerts.discardChanges"/>"/></a>
		<input type="submit" value="<spring:message code="programmablealerts.saveAlert"/>"/>
	</form>
</div>
<br/>

<script type="text/javascript">
	clearError("recipients");
	
	function validateForm() {
		var recipients = document.getElementById("userNames");
		var optUsers=recipients.options;
		if (optUsers.length < 1) {
			document.getElementById("recipientsError").style.display = "";
			result = false;
		}
		return result;
	}

	function clearError(errorName) {
		document.getElementById(errorName + "Error").style.display = "none";
	}
</script>
<%@ include file="/WEB-INF/template/footer.jsp" %>