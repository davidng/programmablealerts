<%@ include file="/WEB-INF/template/include.jsp" %>
<%@ include file="/WEB-INF/template/headerMinimal.jsp" %>

<openmrs:require privilege="View Programmable Alerts Properties" otherwise="/login.htm" />

<c:choose>
	<c:when test="${alert == null}">
		<br/>
		<i>(<spring:message code="programmablealerts.nullAlert"/>)</i>
		<br/>
	</c:when>
	<c:otherwise>
		<b>-<spring:message code="general.name"/>:   </b>${alert.alertName}<br/><br/>
		<b>-<spring:message code="Alert.recipients"/>:   </b>
			<c:forEach items="${recipients}" var="recipient">
				[${recipient.personName}] 
			</c:forEach>
		<br/><br/>
		<b>-<spring:message code="feedback.subject"/>:   </b>${alert.subject}<br/><br/>
		<b>-<spring:message code="programmablealerts.emailBody"/>:   </b>${alert.message}<br/><br/>
	</c:otherwise>
</c:choose>