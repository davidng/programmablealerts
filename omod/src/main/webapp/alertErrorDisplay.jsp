<%@ include file="/WEB-INF/template/include.jsp" %>
<%@ include file="/WEB-INF/template/headerMinimal.jsp" %>

<openmrs:require privilege="View Programmable Alerts Properties" otherwise="/login.htm" />

<c:choose>
	<c:when test="${alert == null}">
		<br/>
		<i>(<spring:message code="programmablealerts.nullAlert"/>)</i>
		<br/>
	</c:when>
	<c:otherwise>
		<b>-<spring:message code="general.name"/>:   </b>${alert.alertName}
		<div style="float: right">
			<openmrs:hasPrivilege privilege="Edit Programmable Alerts">
				<a href="javascript:void(0)" onClick="window.parent.location = '${pageContext.request.contextPath}/module/programmablealerts/alertErrorEdit.form?alertId=${alert.id}'; return false;">[ <spring:message code="programmablealerts.error.correct"/> ]</a>
			</openmrs:hasPrivilege>
		</div>
		<br/><br/>
		<b>-<spring:message code="Alert.recipients"/>:   </b>
			<c:forEach items="${recipients}" var="recipient">
				[${recipient.personName}] 
			</c:forEach>
		<br/><br/>
		<b>-<spring:message code="feedback.subject"/>:   </b>${alert.subject}<br/><br/>
		<b>-<spring:message code="programmablealerts.emailBody"/>:   </b>${alert.message}<br/><br/>
		<b>-<spring:message code="programmablealerts.lastAttempt"/>:   </b>
			<openmrs:formatDate date="${alert.lastAttempt}" type="long" /><br/><br/>
		<b>-<spring:message code="programmablealerts.lastAttemptReport"/>:   </b>
			${alert.lastAttemptReport}
	</c:otherwise>
</c:choose>