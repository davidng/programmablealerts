<ul id="menu">
	<li class="first">
		<a href="${pageContext.request.contextPath}/admin"><spring:message code="admin.title.short"/></a>
	</li>

	<openmrs:hasPrivilege privilege="View Alerts Template">
		<li <c:if test='<%= request.getRequestURI().contains("programmablealerts/alertTemplateList") %>'>class="active"</c:if>>
			<a href="${pageContext.request.contextPath}/module/programmablealerts/alertTemplateList.list">
				<spring:message code="programmablealerts.alertTemplate.title"/>
			</a>
		</li>
	</openmrs:hasPrivilege>
	
	<openmrs:hasPrivilege privilege="View Programmable Alerts">
		<li <c:if test='<%= request.getRequestURI().contains("programmablealerts/alertList") %>'>class="active"</c:if>>
			<a href="${pageContext.request.contextPath}/module/programmablealerts/alertList.list">
				<spring:message code="programmablealerts.programAlert.title"/>
			</a>
		</li>
	</openmrs:hasPrivilege>
	
	<openmrs:hasPrivilege privilege="Manage Alert Errors">
		<li <c:if test='<%= request.getRequestURI().contains("programmablealerts/alertError") %>'>class="active"</c:if>>
			<a href="${pageContext.request.contextPath}/module/programmablealerts/alertErrorList.list">
				<spring:message code="programmablealerts.alertErrors.title"/>
			</a>
		</li>
	</openmrs:hasPrivilege>
	
	<openmrs:hasPrivilege privilege="View Sent Alerts">
		<li <c:if test='<%= request.getRequestURI().contains("programmablealerts/sentAlerts") %>'>class="active"</c:if>>
			<a href="${pageContext.request.contextPath}/module/programmablealerts/sentAlerts.list">
				<spring:message code="programmablealerts.sentAlerts.title"/>
			</a>
		</li>
	</openmrs:hasPrivilege>
	
	<openmrs:hasPrivilege privilege="View Alerts Properties">
		<li <c:if test='<%= request.getRequestURI().contains("programmablealerts/properties") %>'>class="active"</c:if>>
			<a href="${pageContext.request.contextPath}/module/programmablealerts/properties.form">
				<spring:message code="programmablealerts.properties"/>
			</a>
		</li>
	</openmrs:hasPrivilege>
</ul>