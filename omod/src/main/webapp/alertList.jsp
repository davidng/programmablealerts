<%@ include file="/WEB-INF/template/include.jsp" %>

<openmrs:require privilege="View Programmable Alerts Properties" otherwise="/login.htm" redirect="/module/programmablealerts/alertList.list"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>

<h2><spring:message code="programmablealerts.programAlert.header"/></h2>
<a href="programAlert.form">
	<spring:message 
		code="programmablealerts.newAlert">
	</spring:message>
</a>
<br/><br/>
<c:set var="alertListSize" value="${fn:length(programmableAlerts)}" />
<div>
	<b class="boxHeader">
		<spring:message code="programmablealerts.title"/>
	</b>
	<c:choose>
		<c:when test="${alertListSize < 1}">
			<br/>
			<i>(<spring:message code="list.empty"/>)</i>
			<br/>
		</c:when>
		<c:otherwise>
			<div class="box">
				<table cellpadding="8" cellspacing="0" colspan="2">
					<tr>
						<th><spring:message code="general.id"/></th>
						<th><spring:message code="general.name"/></th>
						<th><spring:message code="programmablealerts.template"/></th>
						<th><spring:message code="general.dateCreated"/></th>
						<th></th>
						<th><spring:message code="general.edit"/></th>
						<th></th>
						<th><spring:message code="general.delete"/></th>
					</tr>
					<c:forEach items="${programmableAlerts}" var="alert">
						<form method="post">
							<tr>
								<input type="hidden" name="alertId" value="${alert.id}"/>
								<td>${alert.id}</td>
								<td>${alert.alertName}</td>
								<td>
									<a href="alertTemplate.form?templateId=${alert.template.id}">${alert.template.templateName}</a>
								</td>
								<td><openmrs:formatDate date="${alert.dateCreated}" type="medium" /></td>
								<td></td>
								<td valign="top">
									<a href="programAlert.form?id=${alert.id}">
										<img src="${pageContext.request.contextPath}/images/edit.gif" title="<spring:message code="general.edit"/>" border="0" align="top" />
									 </a>
								</td>
								<td></td>
								<td valign="top">
									<input type="image" src="${pageContext.request.contextPath}/images/trash.gif" 
									onclick="return confirm('<spring:message code="programmablealerts.confirmDelete"/>');" 
									title="<spring:message code="general.delete"/>" 
								</td>
							</tr>
						</form>
					</c:forEach>
				</table>
			</div>
		</c:otherwise>
	</c:choose>
</div>

<br/><br/>
<%@ include file="/WEB-INF/template/footer.jsp" %>