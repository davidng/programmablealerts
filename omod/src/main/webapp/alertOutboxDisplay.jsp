<%@ include file="/WEB-INF/template/include.jsp" %>
<%@ include file="/WEB-INF/template/headerMinimal.jsp" %>

<openmrs:require privilege="View Programmable Alerts Properties" otherwise="/login.htm" />

<c:set var="alertOutboxSize" value="${fn:length(alertOutbox)}" />
<c:choose>
	<c:when test="${alertOutboxSize < 1}">
		<br/>
		<i>(<spring:message code="programmablealerts.nullAlert"/>)</i>
		<br/>
	</c:when>
	<c:otherwise>
		<div class="obsDate">
			<table cellpadding="8" cellspacing="0">
				<thead>
					<tr>
						<th><spring:message code="general.name" /></th>
						<th><spring:message code="Alert.recipients"/></th>
						<th><spring:message code="Patient.title"/></th>
						<th><spring:message code="feedback.subject"/>&<spring:message code="programmablealerts.emailBody"/></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${alertOutbox}" var="alert" varStatus="status">
						<tr class="${status.index % 2 == 0 ? 'evenRow' : 'oddRow' }">
							<td>${alert.alertName}</td>
							<td>${alert.recipientEmailList}</td>
							<td>${alert.patient.personName.fullName}</td>
							<td>${alert.subject}<br/><textarea rows="3" cols="50" readonly="true">${alert.message}</textarea></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</c:otherwise>
</c:choose>