<%@ include file="/WEB-INF/template/include.jsp" %>

<openmrs:require privilege="View Programmable Alerts Properties" otherwise="/login.htm" redirect="/module/programmablealerts/properties.form"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>

<h2><spring:message code="programmablealerts.alertTemplate.header"/></h2>
<a href="alertTemplate.form">
	<spring:message 
		code="programmablealerts.createNewAlertTemplate">
	</spring:message>
</a><br/><br/>
<c:set var="templateListSize" value="${fn:length(alertTemplates)}" />
<div>
	<b class="boxHeader">
		<spring:message code="programmablealerts.alertTemplateList"/>
	</b>
	<c:choose>
		<c:when test="${templateListSize < 1}">
			<br/>
			<i>(<spring:message code="list.empty"/>)</i>
			<br/>
		</c:when>
		<c:otherwise>
			<div class="box">
				<table cellpadding="8" cellspacing="0" colspan="2">
					<tr>
						<th><spring:message code="general.id"/></th>
						<th><spring:message code="general.name"/></th>
						<th><spring:message code="feedback.subject"/></th>
						<th><spring:message code="general.dateCreated"/></th>
						<th></th>
						<th><spring:message code="general.action"/></th>
					</tr>
					<c:forEach items="${alertTemplates}" var="alertTemplate">
						<form method="post">
							<input type="hidden" name="templateId" value="${alertTemplate.id}"/>
							<tr>
								<td>
									<a href="alertTemplate.form?templateId=${alertTemplate.id}">${alertTemplate.id}</a>
								</td>
								<td>
									<a href="alertTemplate.form?templateId=${alertTemplate.id}">${alertTemplate.templateName}</a>
								</td>
								<td>${alertTemplate.subject}</td>
								<td><openmrs:formatDate date="${alertTemplate.dateCreated}" type="medium" /></td>
								<td></td>
								<td valign="top">
									<input type="image" src="${pageContext.request.contextPath}/images/trash.gif" 
									onclick="return confirm('<spring:message code="programmablealerts.confirmDelete"/>');" 
									title="<spring:message code="general.delete"/>" 
								</td>
							</tr>
						</form>
					</c:forEach>
				</table>
			</div>
		</c:otherwise>
	</c:choose>
</div>

<br/><br/>
<%@ include file="/WEB-INF/template/footer.jsp" %>