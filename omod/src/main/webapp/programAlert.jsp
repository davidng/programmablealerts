<%@ include file="/WEB-INF/template/include.jsp" %>

<openmrs:require privilege="View Programmable Alerts Properties" otherwise="/login.htm" redirect="/module/programmablealerts/programAlert.form"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>
<openmrs:htmlInclude file="/moduleResources/programmablealerts/scripts/dojo/dojo.js"/>

<script type="text/javascript">
	dojo.require("dojo.widget.openmrs.UserSearch");
	dojo.require("dojo.widget.openmrs.RoleSearch");
	dojo.require("dojo.widget.openmrs.OpenmrsPopup");
	
	dojo.addOnLoad( function() {
		
		searchWidget = dojo.widget.manager.getWidgetById("uSearch");		
		
		dojo.event.topic.subscribe("uSearch/select", 
			function(msg) {
				for (var i=0; i< msg.objs.length; i++) {
					var obj = msg.objs[i];
					var options = $("userNames").options;
					var isAddable = true;
					for (x=0; x<options.length; x++)
						if (options[x].value == obj.userId)
							isAddable = false;
					
					if (isAddable) {
						var opt = new Option(obj.personName, obj.userId);
						opt.selected = true;
						options[options.length] = opt;
						copyIds("userNames", "userIds", ",");
					}
				}
			}
		);

		dojo.event.topic.subscribe("rSearch/select", 
			function(msg) {
				for (var i=0; i< msg.objs.length; i++) {
					var obj = msg.objs[i];
					var options = $("roleNames").options;
					var isAddable = true;
					for (x=0; x<options.length; x++)
						if (options[x].value == obj.roleId)
							isAddable = false;
					
					if (isAddable) {
						var opt = new Option(obj.name, obj.roleId);
						opt.selected = true;
						options[options.length] = opt;
						copyIds("roleNames", "roleIds", ",");
					}
				}	
			}
		);
	});
	
</script>

<script type="text/javascript">
		
	function removeItem(nameList, idList, delim)
	{
		var sel   = document.getElementById(nameList);
		var input = document.getElementById(idList);
		var optList   = sel.options;
		var lastIndex = -1;
		var i = 0;
		while (i<optList.length) {
			// loop over and erase all selected items
			if (optList[i].selected) {
				optList[i] = null;
				lastIndex = i;
			}
			else {
				i++;
			}
		}
		copyIds(nameList, idList, delim);
		while (lastIndex >= optList.length)
			lastIndex = lastIndex - 1;
		if (lastIndex >= 0) {
			optList[lastIndex].selected = true;
			return optList[lastIndex];
		}
		return null;
	}
	
	function copyIds(from, to, delimiter)
	{
		var sel = document.getElementById(from);
		var input = document.getElementById(to);
		var optList = sel.options;
		var remaining = new Array();
		var i=0;
		while (i < optList.length)
		{
			remaining.push(optList[i].value);
			i++;
		}
		input.value = remaining.join(delimiter);
		
		if (optList.length > 0){
			clearError("recipients");
		}
	}
	
	function listKeyPress(from, to, delim, event) {
		var keyCode = event.keyCode;
		if (keyCode == 8 || keyCode == 46) {
			removeItem(from, to, delim);
			//attempt to prevent backspace key (#8) from going back in browser
			window.Event.keyCode = 0;
		}
	}
</script>

<style type="text/css">
	.duplicateButton {
		font-size: .8em;
		border: 1px solid darkgrey;
		background-color: whitesmoke;
		cursor: pointer;
		margin: 1px;
		padding: 1px;
	}.selectWidth {
		width: 295px;
		}
		select.selectWidth {
			width: 295px;
		}
</style>
<h2><spring:message code="programmablealerts.programNewAlert"/></h2>
<div>
	<b class="boxHeader">
		<spring:message code="programmablealerts.newAlert"/>
	</b>
	<form method="post" onSubmit="return validateForm()">
		<div class="box">
			<input type="hidden" name="alertId" value="${alert.id}"/>
			<table>
				<tr>
					<td>
						<spring:message code="general.name"/>
					</td>
					<td>
						<input type="text" id="alertName" name="alertName" size="40"
							value="${alert.alertName}" onKeyUp="clearError('alertName')"/>
						<span class="error" id="alertNameError">
							<spring:message code="programmablealerts.requiredField"/></span>
					</td>
				</tr>
				<tr>
					<td valign="top"><spring:message code="Alert.recipients"/></td>
					<td valign="top">
						<input type="hidden" name="userIds" id="userIds" size="40" value='<c:forEach items="${recipients}" var="recipient">${recipient.id},</c:forEach>' />
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top">
									<select class="selectWidth" size="6" id="userNames" multiple onkeyup="listKeyPress('userNames', 'userIds', ',', event);">
										<c:forEach items="${recipients}" var="recipient">
											<option value="${recipient.id}">${recipient.personName}</option>
										</c:forEach>
									</select>
								</td>
								<td valign="top" class="buttons">
									&nbsp;<span dojoType="UserSearch" widgetId="uSearch"></span><span dojoType="OpenmrsPopup" searchWidget="uSearch" searchTitle='<spring:message code="programmablealerts.recipients.search"/>' changeButtonValue='<spring:message code="general.add"/>'></span>
									&nbsp; <input type="button" value="<spring:message code="general.remove"/>" class="smallButton" onClick="removeItem('userNames', 'userIds', ',');" /> <br/><br/>
									&nbsp;<span class="error" id="recipientsError">
												<spring:message code="programmablealerts.requiredField"/></span>
								</td>
							</tr>
						</table>
					</td>
					<td style="width:50px;"></td>


					<td valign="top"><spring:message code="Alert.roles"/></td>
					<td valign="top">
						<input type="hidden" name="roleIds" id="roleIds" size="40" value='<c:forEach items="${recipients}" var="recipient">${recipient.id},</c:forEach>' />
						<table cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top">
									<select class="selectWidth" size="6" id="roleNames" multiple onkeyup="listKeyPress('roleNames', 'roleIds', ',', event);">
										<c:forEach items="${roles}" var="role">
											<option value="${role.name}">${role.name}</option>
										</c:forEach>		
									</select>
								</td>
								<td valign="top" class="buttons">
									&nbsp;<span dojoType="RoleSearch" widgetId="rSearch"></span><span dojoType="OpenmrsPopup" searchWidget="rSearch" searchTitle='<spring:message code="programmablealerts.roles.search"/>' changeButtonValue='<spring:message code="general.add"/>'></span>
									&nbsp; <input type="button" value="<spring:message code="general.remove"/>" class="smallButton" onClick="removeItem('roleNames', 'roleIds', ',');" /> <br/><br/>
									&nbsp;
								</td>
							</tr>
						</table>
					</td>

				</tr>
				<tr>
					<td>
						<spring:message code="programmablealerts.template"/>
					</td>
					<td>
						<c:forEach items="${templates}" var="template">
							<input type="hidden" id="template${template.templateId}" name="${template.subject}" value="${template.message}"/>
						</c:forEach>
						<select id="alertTemplate" name="alertTemplate" 
							onKeyUp="clearError('alertTemplate'); showTemplate(this.value);" 
							onclick="clearError('alertTemplate'); showTemplate(this.value);">
							<option value="" selected><spring:message code="general.select"/></option>
							<c:forEach items="${templates}" var="template">
								<option value="${template.templateId}" <c:if test="${alert.template.templateId == template.templateId}">selected</c:if>>${template.templateName}</option>
							</c:forEach>
						</select>
						<span class="error" id="alertTemplateError">
							<spring:message code="programmablealerts.requiredTemplate"/></span>
					</td>
				</tr>
			</table>
			<br/>
			<div id="messageBox" class="box" style="display: none">
				<table>	
					<tr>
						<td><spring:message code="feedback.subject"/></td>
						<td><input type="text" style="background-color: whitesmoke" id="subject" readonly="true" name="subject" size="40"/></td>
					</tr>
					<tr>
						<td valign="top"><spring:message code="programmablealerts.emailBody"/></td>
						<td><textarea style="background-color: whitesmoke" id="emailbody" name="emailbody" readonly="true" rows="5" cols="60"></textarea></td>
					</tr>
					<tr>
						<td>
						<td align="right">
							<input type="button" class="duplicateButton" value="<spring:message code="programmablealerts.duplicateTemplate"/>" onclick="duplicateTemplate()"/></td>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<spring:message code="general.select"/> <spring:message code="programmablealerts.savedSearch"/>:
							<select id="savedSearch" name="savedSearch" 
								onKeyUp="clearError('savedSearch');" onclick="clearError('savedSearch');">
								<option value=""><spring:message code="general.select"/></option>
								<c:forEach items="${savedSearches}" var="savedSearch">
									<option value="${savedSearch.id}" <c:if test="${alert.alertTrigger.savedSearch == savedSearch.id}">selected</c:if>>${savedSearch.name}</option>
								</c:forEach>
							</select>
							<span class="error" id="savedSearchError">
								<spring:message code="programmablealerts.requiredSavedSearch"/></span>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<br/>
		<a href="alertList.list"><input type="button" value="<spring:message code="programmablealerts.discardChanges"/>"/></a>
		<input type="submit" value="<spring:message code="programmablealerts.saveAlert"/>"/>
	</form>
</div>
<br/>

<script type="text/javascript">
	clearError("alertName");
	clearError("alertTemplate");
	clearError("recipients");
	clearError("savedSearch");
	var selTemplate = document.getElementById('alertTemplate').value;
	showTemplate(selTemplate);
	
	function validateForm() {
		var alertName = document.getElementById("alertName");
		var recipients = document.getElementById("userNames");
		var alertTemplate = document.getElementById("alertTemplate");
		var savedSearch = document.getElementById("savedSearch");
		
		var result = true;
		if (alertName.value == "") {
			document.getElementById("alertNameError").style.display = "";
			result = false;
		}
		
//		var optUsers=recipients.options;
//		if (optUsers.length < 1) {
//			document.getElementById("recipientsError").style.display = "";
//			result = false;
//		}
		
		if (alertTemplate.value == "") {
			document.getElementById("alertTemplateError").style.display = "";
			result = false;
		}
		
		if (savedSearch.value == "") {
			document.getElementById("savedSearchError").style.display = "";
			result = false;
		}
		
		return result;
	}

	function clearError(errorName) {
		document.getElementById(errorName + "Error").style.display = "none";
	}
	
	function showTemplate(id) {
		var template = document.getElementById('alertTemplate').value;
		if (template == '' && id == '')
			document.getElementById('messageBox').style.display = "none";
		else{
			document.getElementById('messageBox').style.display = "";
			var subject = document.getElementById("template" + id).name;
			var message = document.getElementById("template" + id).value;
			document.getElementById('subject').value = subject;
			document.getElementById('emailbody').value = message;
			if (template == '')
				document.getElementById('alertTemplate').selectedIndex=id;
		}
	}
	
	function duplicateTemplate() {
		selTemplateId = document.getElementById('alertTemplate').value;
		window.location = "alertTemplate.form?duplicate=true&templateId=" + selTemplateId;
	}
	
</script>
<%@ include file="/WEB-INF/template/footer.jsp" %>
