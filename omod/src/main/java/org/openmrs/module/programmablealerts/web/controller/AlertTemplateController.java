package org.openmrs.module.programmablealerts.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.openmrs.GlobalProperty;
import org.openmrs.PersonAttributeType;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.programmablealerts.AlertTemplate;
import org.openmrs.module.programmablealerts.ProgrammableAlertsService;
import org.openmrs.web.WebConstants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for alert template page jsp
 * 
 * @author Samuel Mbugua
 */
@Controller
public class AlertTemplateController {
	private ProgrammableAlertsService alertsService;
	
	/**
	 * Controller for alert templates list jsp Page
	 */
	@ModelAttribute("alertTemplates")	
	@RequestMapping(value="/module/programmablealerts/alertTemplateList", method=RequestMethod.GET)
	public List<AlertTemplate> populateForm() {
		getAlertsService();
		return alertsService.getAllAlertTemplates();
	}
	
	/**
	 * Controller for alert template list post jsp Page
	 */
	@RequestMapping(value="/module/programmablealerts/alertTemplateList", method=RequestMethod.POST)
	public String deleteTemplate(HttpSession httpSession, @RequestParam Integer templateId) {
		getAlertsService();
		try {
			if (templateId != null){
				alertsService.deleteTemplate(alertsService.getAlertTemplate(templateId));
				String msg = Context.getMessageSourceService().getMessage("programmablealerts.delete.success");
				httpSession.setAttribute(WebConstants.OPENMRS_MSG_ATTR, msg + templateId);
			}else {
				String msg = Context.getMessageSourceService().getMessage("programmablealerts.template.delete.error");
				httpSession.setAttribute(WebConstants.OPENMRS_ERROR_ATTR, msg);
			}
		}catch (Exception e) {
			String msg = Context.getMessageSourceService().getMessage("programmablealerts.template.delete.error");
			httpSession.setAttribute(WebConstants.OPENMRS_ERROR_ATTR, msg);
		}
		return "redirect:alertTemplateList.list";		
	}
	
	/**
	 * Controller for alert template jsp Page
	 */
	@RequestMapping(value="/module/programmablealerts/alertTemplate", method=RequestMethod.GET)
	public String getTemplate(ModelMap model, HttpServletRequest request) {
		getAlertsService();
		String templateId=request.getParameter("templateId");
		String duplicate=request.getParameter("duplicate");
		AlertTemplate alertTemplate = new AlertTemplate();
		if (templateId != null && templateId.trim() != ""){
			Integer id = Integer.parseInt(templateId);
			alertTemplate= alertsService.getAlertTemplate(id);
			if (duplicate != null && duplicate.equalsIgnoreCase("true")){
				alertTemplate.setId(null);
				alertTemplate.setTemplateName("");
			}
		}
		model.addAttribute("alertTemplate", alertTemplate);
		
		//here we create the insertables
		AdministrationService as = Context.getAdministrationService();
		List<GlobalProperty> props = as.getGlobalPropertiesByPrefix("programmablealerts.concept");
		model.addAttribute("props", props);
		List<String> ptypesName = new ArrayList<String>();
		List<PersonAttributeType> ptypes = Context.getPersonService().getAllPersonAttributeTypes();
		for (PersonAttributeType type : ptypes){
			if (! type.isRetired())
				ptypesName.add(type.getName());
		}
		model.addAttribute("ptypes", ptypesName);
		return "/module/programmablealerts/alertTemplate" ;
	}
	
	/**
	 * Controller for alert template post jsp Page
	 */
	@RequestMapping(value="/module/programmablealerts/alertTemplate", method=RequestMethod.POST)
	public String saveTemplate(HttpSession httpSession, @RequestParam String alertTemplateName,
			@RequestParam String subject, @RequestParam String emailBody, @RequestParam Integer alertTemplateId) {
		getAlertsService();
		AlertTemplate alertTemplate = alertsService.getAlertTemplateByName(alertTemplateName);
		
		//Ensure no duplicate name
		if (alertTemplate != null){
			//if alertTemplateId is null (new) or we have two different templateIds we throw an error
			if (alertTemplateId==null || alertTemplateId != alertTemplate.getId().intValue()) {
				String msg=Context.getMessageSourceService().getMessage("programmablealerts.create.error");
				httpSession.setAttribute(WebConstants.OPENMRS_ERROR_ATTR, msg);
				return null;
			}
		}else{
			alertTemplate = alertsService.getAlertTemplate(alertTemplateId);
			if (alertTemplate == null)
				alertTemplate = new AlertTemplate();
		}
			
		alertTemplate.setTemplateName(alertTemplateName);
		alertTemplate.setSubject(subject);
		alertTemplate.setMessage(emailBody);
		alertsService.saveTemplate(alertTemplate);
		String msg=Context.getMessageSourceService().getMessage("programmablealerts.create.success");
		httpSession.setAttribute(WebConstants.OPENMRS_MSG_ATTR, msg);
		return "redirect:alertTemplateList.list";		
	}
	
	private void getAlertsService(){
		if (alertsService == null)
			alertsService=(ProgrammableAlertsService)Context.getService(ProgrammableAlertsService.class);
	}
}
