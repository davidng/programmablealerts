package org.openmrs.module.programmablealerts.extension.html;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import org.openmrs.module.Extension;
import org.openmrs.module.web.extension.AdministrationSectionExt;
import org.openmrs.util.OpenmrsClassLoader;

/**
 * Anchor for the module in the Main OpenMRS administration page
 * 
 * @author Samuel Mbugua
 *
 */
@SuppressWarnings("deprecation")
public class AdminList extends AdministrationSectionExt {
	
	private static String requiredPrivileges = "View Email Alerts";
	

	public Extension.MEDIA_TYPE getMediaType() {
		return Extension.MEDIA_TYPE.html;
	}
	

	public String getTitle() {
		return "programmablealerts.title";
	}
	
	public String getRequiredPrivilege() {
		if (requiredPrivileges == null) {
			StringBuilder builder = new StringBuilder();
			requiredPrivileges = builder.toString();
		}
		
		return requiredPrivileges;
	}
	
	public Map<String, String> getLinks() {		
		//Map<String, String> map = new TreeMap<String, String>(new InsertedOrderComparator());
				Map<String, String> map = new LinkedHashMap<String, String>();
				Thread.currentThread().setContextClassLoader(OpenmrsClassLoader.getInstance());
		map.put("module/programmablealerts/alertTemplateList.list", "programmablealerts.alertTemplate.title");
		map.put("module/programmablealerts/alertList.list", "programmablealerts.programAlert.title");
		map.put("module/programmablealerts/alertErrorList.list", "programmablealerts.alertErrors.title");
		map.put("module/programmablealerts/sentAlerts.list", "programmablealerts.sentAlerts.title");
		map.put("module/programmablealerts/properties.form", "programmablealerts.properties");
		return map;
	}
	
}
