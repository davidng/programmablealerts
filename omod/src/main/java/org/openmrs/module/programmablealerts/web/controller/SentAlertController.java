package org.openmrs.module.programmablealerts.web.controller;

import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.openmrs.User;
import org.openmrs.api.context.Context;
import org.openmrs.module.programmablealerts.AlertArchive;
import org.openmrs.module.programmablealerts.AlertOutbox;
import org.openmrs.module.programmablealerts.ProgrammableAlertsService;
import org.openmrs.module.programmablealerts.util.ProgrammableAlertsUtils;
import org.openmrs.util.OpenmrsConstants;
import org.openmrs.web.WebConstants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for sent alert jsp page 
 * 
 * @author Samuel Mbugua
 */
@Controller
public class SentAlertController {
	
	private ProgrammableAlertsService alertsService;
	
	@RequestMapping(value="/module/programmablealerts/sentAlerts", method=RequestMethod.GET)
	public String populateForm(ModelMap model, HttpServletRequest request) {
		getAlertsService();
		Integer length = 30;
		Integer page = null;
		boolean endPage=false;
		try {
			page = Integer.parseInt(request.getParameter("page"));
		}catch (NumberFormatException e) {
			page=0;
		}
		
		Integer nextPage = page+1;
		List<AlertArchive> alertArchives = alertsService.getAllAlertArchive(nextPage*length, length);
		if (alertArchives.size() < 1 || alertArchives == null)
			endPage=true;
		model.addAttribute("sentAlerts", alertsService.getAllAlertArchive(page*length, length));
		model.addAttribute("endPage", endPage);
		model.addAttribute("page", nextPage);
		return "/module/programmablealerts/sentAlerts";
	}
	
	@RequestMapping(value="/module/programmablealerts/alertDisplay", method=RequestMethod.GET)
	public String showAlert(ModelMap model, HttpServletRequest request) {
		getAlertsService();
		String alertId=request.getParameter("alertId");
		AlertArchive alertArchive =  alertsService.getAlertArchive(Integer.parseInt(alertId));
		List<User> recipients = null;
		if (alertArchive != null) {
			recipients = ProgrammableAlertsUtils.getRecipients(alertArchive.getRecipients());
			List<User> recipients2 = ProgrammableAlertsUtils.getRecipientsByRoles(alertArchive.getRoles());
			ArrayList<Integer> userIds = new ArrayList<Integer>();
			for(User user : recipients) userIds.add(user.getId());
			for(User user : recipients2){
				if(!userIds.contains(user.getId())){
					recipients.add(user);
					userIds.add(user.getId());
				}
			}
		}
		model.addAttribute("alert", alertArchive);
		model.addAttribute("recipients", recipients);
		return "/module/programmablealerts/alertDisplay";
	}
	
	@ModelAttribute("alertOutbox")
	@RequestMapping(value="/module/programmablealerts/alertOutboxDisplay", method=RequestMethod.GET)
	public List<AlertOutbox> showOutboxList(HttpServletRequest request) {
		getAlertsService();
		
		List<AlertOutbox> alertOutboxs = alertsService.getAllAlertsInOutbox();
		for (AlertOutbox alertOutbox : alertOutboxs) {
			List<User> users = ProgrammableAlertsUtils.getRecipients(alertOutbox.getRecipients());
			List<User> users2 = ProgrammableAlertsUtils.getRecipientsByRoles(alertOutbox.getRoles());
			ArrayList<Integer> userIds = new ArrayList<Integer>();
			for(User user : users) userIds.add(user.getId());
			for(User user : users2){
				if(!userIds.contains(user.getId())){
					users.add(user);
					userIds.add(user.getId());
				}
			}

			String recipientEmailList = "";
			for (User user : users) {
				try{
					recipientEmailList = recipientEmailList + user.getUserProperty(OpenmrsConstants.USER_PROPERTY_NOTIFICATION_ADDRESS) + ",";
				}catch(Exception e){
					request.getSession().setAttribute(WebConstants.OPENMRS_ERROR_ATTR, "Error processing request, user should not be null");
				}
			}
			recipientEmailList=recipientEmailList.substring(0,recipientEmailList.lastIndexOf(","));
			request.getSession().setAttribute(WebConstants.OPENMRS_MSG_ATTR, "We will send alert email to the following recipients: \n" + recipientEmailList);
			alertOutbox.setRecipientEmailList(recipientEmailList);
		}
		return alertOutboxs;
	}
	
	private void getAlertsService(){
		if (alertsService == null)
			alertsService=(ProgrammableAlertsService)Context.getService(ProgrammableAlertsService.class);
	}
}
