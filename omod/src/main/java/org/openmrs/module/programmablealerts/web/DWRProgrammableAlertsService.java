/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.openmrs.module.programmablealerts.web;

import java.util.List;
import java.util.Vector;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.openmrs.Role;
import org.openmrs.api.APIException;

import org.openmrs.api.context.Context;
import org.openmrs.messagesource.MessageSourceService;
import org.openmrs.module.programmablealerts.ProgrammableAlertsService;
import org.openmrs.module.programmablealerts.RoleListItem;



/**
 *
 * @author guess
 */
public class DWRProgrammableAlertsService {
    
    private static final Log log = LogFactory.getLog(DWRProgrammableAlertsService.class);
    
    public Vector<Object> findBatchOfRoles(String searchValue, Integer start, Integer length)
	        throws APIException {
		
		Vector<Object> roleList = new Vector<Object>();
		MessageSourceService mss = Context.getMessageSourceService();
		
		try {                        
                        ProgrammableAlertsService ps = Context.getService(ProgrammableAlertsService.class);
			List<Role> roles = ps.getRoles(searchValue, start, length);
			roleList = new Vector<Object>(roles.size());
			
			for (Role role : roles) {
				roleList.add(new RoleListItem(role));
			}
		}
		catch (Exception e) {
			log.error(e);
			//roleList.add(mss.getMessage("Role.search.error") + " - " + e.getMessage());
		}
		
		if (roleList.size() == 0) {
			//roleList.add(mss.getMessage("Role.noLocationsFound"));
		}
		
		return roleList;
	}
    
    public RoleListItem getRole(Integer roleId) {
                ProgrammableAlertsService ps = Context.getService(ProgrammableAlertsService.class);
		Role r = ps.getRole(roleId);		
		return r == null ? null : new RoleListItem(r);
	}
    
    public Vector findRoles(String searchValue) {
		
		return findBatchOfRoles(searchValue, null, null);
	}
 
    public Vector getAllRoles(){
        Vector roleList = new Vector();
		MessageSourceService mss = Context.getMessageSourceService();
		
		try {                        
                        ProgrammableAlertsService ps = Context.getService(ProgrammableAlertsService.class);
			List<Role> roles = ps.getAllRoles();
                        roleList = new Vector(roles.size());
			for (Role role : roles) {                                
                                roleList.add(new RoleListItem(role));                                
			}
		}
		catch (Exception e) {
			log.error(e);
			//roleList.add(mss.getMessage("Role.search.error") + " - " + e.getMessage());
		}
		
		if (roleList.size() == 0) {
			//roleList.add(mss.getMessage("Role.noLocationsFound"));
		}
		
		return roleList;
    }
}
