package org.openmrs.module.programmablealerts.web.controller;

import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.openmrs.Role;
import org.openmrs.User;
import org.openmrs.api.context.Context;
import org.openmrs.messagesource.MessageSourceService;
import org.openmrs.module.programmablealerts.AlertQueue;
import org.openmrs.module.programmablealerts.AlertTrigger;
import org.openmrs.module.programmablealerts.ProgrammableAlertsService;
import org.openmrs.module.programmablealerts.util.ProgrammableAlertsUtils;
import org.openmrs.module.reporting.cohort.definition.CohortDefinition;
import org.openmrs.module.reporting.cohort.definition.service.CohortDefinitionService;
import org.openmrs.util.OpenmrsConstants;
import org.openmrs.web.WebConstants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for program alert jsp page 
 * 
 * @author Samuel Mbugua
 */
@Controller
public class ProgramAlertController {
	
	private ProgrammableAlertsService alertsService;
	private AlertQueue locAlertQueue;
	
	/**
	 * @return a list of {@link AlertQueue}
	 */
	@ModelAttribute("programmableAlerts")
	@RequestMapping(value="/module/programmablealerts/alertList", method=RequestMethod.GET)
	public List<AlertQueue> populateForm() {
		getAlertsService();
		return alertsService.getAllAlertsInQueue();
	}
	
	/**
	 * Controller for alert list post jsp Page
	 */
	@RequestMapping(value="/module/programmablealerts/alertList", method=RequestMethod.POST)
	public String deleteAlert(HttpSession httpSession, @RequestParam Integer alertId) {
		MessageSourceService msa = Context.getMessageSourceService();
		getAlertsService();
		AlertQueue alertQueue = alertsService.getAlert(alertId);
		try {
			if (alertQueue != null){
				alertsService.deleteAlertQueue(alertsService.getAlert(alertId));
				httpSession.setAttribute(WebConstants.OPENMRS_MSG_ATTR, 
						msa.getMessage("programmablealerts.delete.success") + alertQueue.getAlertName());
			}else {
				httpSession.setAttribute(WebConstants.OPENMRS_ERROR_ATTR, 
						msa.getMessage("programmablealerts.alert.delete.error") + alertId);
			}
		}catch (Exception e) {
			httpSession.setAttribute(WebConstants.OPENMRS_ERROR_ATTR, 
					msa.getMessage("programmablealerts.alert.delete.error") + alertQueue.getAlertName());
		}
		return "redirect:alertList.list";		
	}
		
	/**
	 * Controller for alert queue jsp Page
	 */
	@RequestMapping(value="/module/programmablealerts/programAlert", method=RequestMethod.GET)
	public String showPage(ModelMap model, HttpServletRequest request) {
		
		getAlertsService();
		String alertId=request.getParameter("id");
		AlertQueue alertQueue = null; 
		List<User> recipients = null;
		List<Role> roles = null;
		if (alertId != null && alertId.trim() != ""){
			Integer id = Integer.parseInt(alertId);
			alertQueue = alertsService.getAlert(id);
		}else
			alertQueue=getLocAlertQueue();
		
		if (alertQueue !=null){
			//get recipients
			String userIds=alertQueue.getRecipients();
			recipients = ProgrammableAlertsUtils.getRecipients(userIds);

			String roleIds = alertQueue.getRoles();
			roles = ProgrammableAlertsUtils.getRoles(roleIds);
			
		}
		//nullify the saved object 
		setLocAlertQueue(null);
		model.addAttribute("alert", alertQueue);
		model.addAttribute("recipients", recipients);
		model.addAttribute("roles", roles);
		model.addAttribute("templates", alertsService.getAllAlertTemplates());
		
		//get saved searches
		model.addAttribute("savedSearches", alertsService.getAllCohortDefinition());
		return "/module/programmablealerts/programAlert";
	}
	
	/**
	 * Controller for alert template post jsp Page
	 */
	@RequestMapping(value="/module/programmablealerts/programAlert", method=RequestMethod.POST)
	public String saveAlert(HttpSession httpSession, @RequestParam String alertName, @RequestParam String userIds, @RequestParam String roleIds, 
			@RequestParam Integer alertTemplate, @RequestParam Integer savedSearch, @RequestParam Integer alertId) {
		getAlertsService();
		AlertQueue alertQueue = alertsService.getAlertByName(alertName);
		MessageSourceService msa = Context.getMessageSourceService();
		StringBuffer success = new StringBuffer();
		boolean duplicate = false;

		//before saving ensure users have valid emails.
		//System.out.println("userIds = " + userIds);
		//System.out.println("roleIds = " + roleIds);

		List<User> lstUsers = ProgrammableAlertsUtils.getRecipients(userIds);
		List<Integer> lstUserIds = new ArrayList<Integer>();
		
		//System.out.print("lstUsers = ");
		if(lstUsers != null)
		{
			for(User u : lstUsers){
				//System.out.print(u.getId() + ",");
				lstUserIds.add(u.getId());
			}
		}

		List<User> lstTotalUsers = new ArrayList<User>();
		lstTotalUsers.addAll(lstUsers);
		
		// get users by roles
		List<User> lstUsers2 = ProgrammableAlertsUtils.getRecipientsByRoles(roleIds);

		//System.out.print("lstUsers2 = ");
		if(lstUsers2 != null)
		{
			for(User u : lstUsers2){
				//System.out.print(u.getId() + ",");
				if(!lstUserIds.contains(u.getId())){
					lstTotalUsers.add(u);
					lstUserIds.add(u.getId());
				}
			}
		}

		
		//System.out.print("lstUsers final = ");
		for(User u : lstTotalUsers){
			//System.out.print(u.getId() + ",");
		}
		
		List<User> lstValidUsers = new ArrayList<User>();
		String users="";
		for (User user : lstTotalUsers) {
			if (user != null) {
				String email = user.getUserProperty(OpenmrsConstants.USER_PROPERTY_NOTIFICATION_ADDRESS);
				if ( email != null && email.trim().length() >= 5)
					lstValidUsers.add(user);
					//users = users + user.getId() + ",";
				else {
					Object [] args = new Object[] {user.getPersonName().getFullName()};
					success.append(msa.getMessage("programmablealerts.error.invalidEmail", args, null) + "<br/>");
				}
			}
		}
		
		//Ensure no duplicate name
		if (alertQueue != null){
			//if alertId is null (new) or we have two different alertIds we throw an error
			if (alertId==null || alertId != alertQueue.getId().intValue())
				duplicate=true;
		}else{
			alertQueue = alertsService.getAlert(alertId);
			if (alertQueue == null)
				alertQueue = new AlertQueue();
		}
		
		alertQueue.setAlertName(alertName.trim());
		alertQueue.setTemplate(alertsService.getAlertTemplate(alertTemplate));
		
		//here we create a trigger
		AlertTrigger alertTrigger= alertsService.getTriggerByField("savedSearch", savedSearch);
		if (alertTrigger == null){
			alertTrigger = new AlertTrigger();
			alertTrigger.setSavedSearch(savedSearch);
			alertsService.saveTrigger(alertTrigger);
			alertTrigger= alertsService.getTriggerByField("savedSearch", savedSearch);
		}
		alertQueue.setAlertTrigger(alertTrigger);
		
		//if (users.trim().length()==0){
		if(lstValidUsers.size() == 0){
			//no valid users, alert not saved
			httpSession.setAttribute(WebConstants.OPENMRS_ERROR_ATTR, 
					msa.getMessage("programmablealerts.error.noValidUser"));
			setLocAlertQueue(alertQueue);
			return "redirect:programAlert.form";
		}else{			
			for(User user : lstValidUsers){
				if(lstUsers.contains(user)){
					users = users + user.getId() + ",";
				}
			}
			//remove last comma (,)
			int index = users.lastIndexOf(',');
			if(index >= 0)users=users.substring(0,index);
		}
		alertQueue.setRecipients(users.replaceAll(" ",""));

		List<Role> lstRoles = ProgrammableAlertsUtils.getRoles(roleIds);
		String roles = "";
		for(Role role : lstRoles){
			roles = roles + role.getRole() + ",";
		}
		roles = roles.substring(0,roles.lastIndexOf(','));
		alertQueue.setRoles(roles);

		if (duplicate){
			httpSession.setAttribute(WebConstants.OPENMRS_ERROR_ATTR, 
					msa.getMessage("programmablealerts.create.error"));
			alertQueue.setId(null);
			setLocAlertQueue(alertQueue);
			return "redirect:programAlert.form";
		}
		
		alertsService.saveAlert(alertQueue);
		
		if (!success.toString().equals("")) { 
			httpSession.setAttribute(WebConstants.OPENMRS_ERROR_ATTR, success.toString());
		}else {
			httpSession.setAttribute(WebConstants.OPENMRS_MSG_ATTR, 
					msa.getMessage("programmablealerts.create.success"));
		}
		return "redirect:alertList.list";		
	}
	
	private void getAlertsService(){
		if (alertsService == null)
			alertsService=(ProgrammableAlertsService)Context.getService(ProgrammableAlertsService.class);
	}

	/**
	 * @return the locAlertQueue
	 */
	public AlertQueue getLocAlertQueue() {
		return locAlertQueue;
	}

	/**
	 * @param locAlertQueue the locAlertQueue to set
	 */
	public void setLocAlertQueue(AlertQueue locAlertQueue) {
		this.locAlertQueue = locAlertQueue;
	}
}
