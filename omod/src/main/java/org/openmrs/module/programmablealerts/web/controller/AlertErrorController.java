package org.openmrs.module.programmablealerts.web.controller;

import java.util.List;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.openmrs.User;
import org.openmrs.api.context.Context;
import org.openmrs.messagesource.MessageSourceService;
import org.openmrs.module.programmablealerts.AlertError;
import org.openmrs.module.programmablealerts.ProgrammableAlertsService;
import org.openmrs.module.programmablealerts.util.ProgrammableAlertsUtils;
import org.openmrs.web.WebConstants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for alert errors jsp page 
 * 
 * @author Samuel Mbugua
 */
@Controller
public class AlertErrorController {
	
	private ProgrammableAlertsService alertsService;
	
	@RequestMapping(value="/module/programmablealerts/alertErrorList", method=RequestMethod.GET)
	public String populateForm(ModelMap model, HttpServletRequest request) {
		getAlertsService();
		Integer length = 30;
		Integer page = null;
		boolean endPage=false;
		try {
			page = Integer.parseInt(request.getParameter("page"));
		}catch (NumberFormatException e) {
			page=0;
		}
		
		Integer nextPage = page+1;
		List<AlertError> alertErrors = alertsService.getAllAlertErrors(nextPage*length, length);
		if (alertErrors.size() < 1 || alertErrors == null)
			endPage=true;
		model.addAttribute("alertErrors", alertsService.getAllAlertErrors(page*length, length));
		model.addAttribute("endPage", endPage);
		model.addAttribute("page", nextPage);
		return "/module/programmablealerts/alertErrorList";
	}
	
	@RequestMapping(value="/module/programmablealerts/alertErrorList", method=RequestMethod.POST)
	public String deleteErrors(HttpSession httpSession,
			@RequestParam("errorId") List<Integer> selectedIds) {
		MessageSourceService msa = Context.getMessageSourceService();
		getAlertsService();
		String deletedIds="";
		for (Integer id : selectedIds) {
			if (alertsService.deleteError(alertsService.getAlertError(id)))
				deletedIds += " " + id;		
		}
		if (deletedIds !="")
			httpSession.setAttribute(WebConstants.OPENMRS_MSG_ATTR,
					msa.getMessage("programmablealerts.delete.success") + deletedIds);
		else
			httpSession.setAttribute(WebConstants.OPENMRS_ERROR_ATTR, 
					msa.getMessage("programmablealerts.delete.error"));
		
		return "redirect:alertErrorList.list";
	}
	
	@RequestMapping(value="/module/programmablealerts/alertErrorDisplay", method=RequestMethod.GET)
	public String showAlert(ModelMap model, HttpServletRequest request) {
		getAlertsService();
		String alertId=request.getParameter("alertId");
		AlertError alertError =  alertsService.getAlertError(Integer.parseInt(alertId));
		List<User> recipients = null;
		if (alertError != null) {
			recipients = ProgrammableAlertsUtils.getRecipients(alertError.getRecipients());
			List<User> recipients2 = ProgrammableAlertsUtils.getRecipientsByRoles(alertError.getRoles());
			ArrayList<Integer> userIds = new ArrayList<Integer>();
			for(User user : recipients) userIds.add(user.getId());
			for(User user : recipients2){
				if(!userIds.contains(user.getId())){
					recipients.add(user);
					userIds.add(user.getId());
				}
			}
		}
		model.addAttribute("alert", alertError);
		model.addAttribute("recipients", recipients);
		return "/module/programmablealerts/alertErrorDisplay";
	}
	
	@RequestMapping(value="/module/programmablealerts/alertErrorEdit", method=RequestMethod.GET)
	public String editAlert(ModelMap model, HttpServletRequest request) {
		getAlertsService();
		String alertId=request.getParameter("alertId");
		AlertError alertError =  alertsService.getAlertError(Integer.parseInt(alertId));
		List<User> recipients = null;
		if (alertError != null) {
			recipients = ProgrammableAlertsUtils.getRecipients(alertError.getRecipients());
			List<User> recipients2 = ProgrammableAlertsUtils.getRecipientsByRoles(alertError.getRoles());
			ArrayList<Integer> userIds = new ArrayList<Integer>();
			for(User user : recipients) userIds.add(user.getId());
			for(User user : recipients2){
				if(!userIds.contains(user.getId())){
					recipients.add(user);
					userIds.add(user.getId());
				}
			}
		}
		model.addAttribute("alert", alertError);
		model.addAttribute("recipients", recipients);
		return "/module/programmablealerts/alertErrorEdit";
	}
	
	@RequestMapping(value="/module/programmablealerts/alertErrorEdit", method=RequestMethod.POST)
	public String saveEdittedAlert(HttpSession httpSession,
			@RequestParam Integer alertId){
		getAlertsService();
		AlertError alertError =  alertsService.getAlertError(alertId);
		alertsService.saveOutbox(ProgrammableAlertsUtils.getOutboxFromError(alertError));
		alertsService.deleteError(alertError);
		return "redirect:alertErrorList.list";
	}
	
	
	private void getAlertsService(){
		if (alertsService == null)
			alertsService=(ProgrammableAlertsService)Context.getService(ProgrammableAlertsService.class);
	}
}
