package org.openmrs.module.programmablealerts.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Cohort;
import org.openmrs.Concept;
import org.openmrs.Encounter;
import org.openmrs.GlobalProperty;
import org.openmrs.Location;
import org.openmrs.Obs;
import org.openmrs.Patient;
import org.openmrs.PersonAttribute;
import org.openmrs.PersonAttributeType;
import org.openmrs.User;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.ConceptService;
import org.openmrs.api.LocationService;
import org.openmrs.api.ObsService;
import org.openmrs.api.PatientService;
import org.openmrs.api.UserService;
import org.openmrs.api.context.Context;
import org.openmrs.cohort.CohortSearchHistory;
import org.openmrs.module.programmablealerts.AlertArchive;
import org.openmrs.module.programmablealerts.AlertError;
import org.openmrs.module.programmablealerts.AlertOutbox;
import org.openmrs.module.programmablealerts.AlertQueue;
import org.openmrs.module.reporting.cohort.definition.CohortDefinition;
import org.openmrs.module.reporting.cohort.definition.service.CohortDefinitionService;
import org.openmrs.module.reporting.evaluation.EvaluationContext;
import org.openmrs.module.reporting.evaluation.EvaluationException;
import org.openmrs.reporting.PatientSearch;
import org.openmrs.reporting.PatientSearchReportObject;
import org.openmrs.reporting.ReportObjectService;
import org.openmrs.util.OpenmrsConstants;
import org.openmrs.Role;

/**
 * Provides utilities needed when processing alerts.
 * 
 * @author Samuel Mbugua
 */

@SuppressWarnings("deprecation")
public class ProgrammableAlertsUtils {
	private static Log log = LogFactory.getLog(ProgrammableAlertsUtils.class);
	private static UserService us = Context.getUserService();
	public static List<User> getRecipients(String userIds) {
		//log.warn("Creating users from Ids");
		if (userIds != null && userIds.trim() != "") {
			List<User> users=new ArrayList<User>();
			userIds=userIds.replaceAll(" ", "");
			String ids[] = userIds.split(",");
			for (String stringId : ids) {
				try{
					Integer id = Integer.parseInt(stringId);
					users.add(us.getUser(id));
				}catch(Exception e){log.error(e);}
			}
			return users;
		}
		return new ArrayList<User>();
	}

	public static List<User> getRecipientsByRoles(String roleIds){
		if(roleIds != null && roleIds.trim() != ""){
			List<User> users = new ArrayList<User>();
			String[] roles = roleIds.split(",");
			for(String role:roles){
				Role r = us.getRole(role);
				if(r != null){
					List<User> tmpUser = us.getUsersByRole(r);
					if(tmpUser != null) users.addAll(tmpUser);
				}
			}
			return users;
		}
		return new ArrayList<User>();
	}

	public static List<Role> getRoles(String roleIds) {
		if(roleIds != null && roleIds.trim() != ""){
			List<Role> roles = new ArrayList<Role>();
			String[] names = roleIds.split(",");
			for(String r:names){
				Role role = us.getRole(r);
				if(role != null) roles.add(role);
			}
			return roles;
		}
		return new ArrayList<Role>();
	}
	
	public static Integer getSendAttempts(String strAttempts){
		try {
			Integer attempt = Integer.parseInt(strAttempts);
			if (attempt <= 10 && attempt  >= 1)
				return attempt;
		}catch (NumberFormatException e) {
		}
		return 5;
	}
	public static List<Patient> getSavedSearchPatientSetFromCohortDefinition(Integer savedSearchId) {
		//log.warn("in getSavedSearch");
		List<Patient> result = null;
		CohortDefinitionService cohortSrv = Context.getService(CohortDefinitionService.class);
		
		CohortDefinition cohort = cohortSrv.getDefinition(CohortDefinition.class, savedSearchId);
		
		try {
			Cohort c = cohortSrv.evaluate(cohort, new EvaluationContext());
			result = Context.getPatientSetService().getPatients(c.getMemberIds());
		} catch (EvaluationException e) {			
			e.printStackTrace();
		}
		
		return result;
	}
	@Deprecated
	public static List<Patient> getSavedSearchPatientSet(Integer savedSearchId) {
		//log.warn("in getpatientset");
		ReportObjectService rs = (ReportObjectService) Context.getService(ReportObjectService.class);
		PatientService ps  = Context.getPatientService();
		CohortSearchHistory history= new CohortSearchHistory();
		List<Patient> patients = new ArrayList<Patient>();
		PatientSearchReportObject ro = (PatientSearchReportObject) rs.getReportObject(savedSearchId);
		if (ro != null){
			history.addSearchItem(PatientSearch.createSavedSearchReference(savedSearchId));
			Cohort ch = history.getPatientSet(0, null);
			Set<Integer> memberIds = ch.getMemberIds();
			for (Integer memberId : memberIds)
				patients.add(ps.getPatient(memberId));
		}
		return patients;
	}
	
	public static AlertOutbox getOutboxFromQueue(AlertQueue alertQueue, Patient patient) {
		AlertOutbox alertOutbox = new AlertOutbox();
		alertOutbox.setAlertName(alertQueue.getAlertName());
		alertOutbox.setAlertTrigger(alertQueue.getAlertTrigger());
		alertOutbox.setSubject(alertQueue.getTemplate().getSubject());
		alertOutbox.setMessage(ProgrammableAlertsUtils.createEmailBody(
							alertQueue.getTemplate().getMessage(), patient));
		alertOutbox.setPatient(patient);
		alertOutbox.setRecipients(alertQueue.getRecipients());
		alertOutbox.setRoles(alertQueue.getRoles());
		return alertOutbox;
	}
	
	public static AlertArchive getArchiveFromOutbox(AlertOutbox alertOutbox){
		AlertArchive alertArchive = new AlertArchive();
		alertArchive.setAlertName(alertOutbox.getAlertName());
		alertArchive.setAlertTrigger(alertOutbox.getAlertTrigger());
		alertArchive.setDateSent(new Date());
		alertArchive.setPatient(alertOutbox.getPatient());
		alertArchive.setMessage(alertOutbox.getMessage());
		alertArchive.setSubject(alertOutbox.getSubject());
		alertArchive.setRecipients(alertOutbox.getRecipients());
		alertArchive.setRoles(alertOutbox.getRoles());
		return alertArchive;
	}
	
	public static AlertError getErrorFromOutbox(AlertOutbox alertOutbox){
		AlertError alertError = new AlertError();
		alertError.setAlertName(alertOutbox.getAlertName());
		alertError.setAlertTrigger(alertOutbox.getAlertTrigger());
		alertError.setLastAttempt(new Date());
		alertError.setLastAttemptReport(ProgrammableAlertsConstants.errorMessage);
		alertError.setPatient(alertOutbox.getPatient());
		alertError.setMessage(alertOutbox.getMessage());
		alertError.setSubject(alertOutbox.getSubject());
		alertError.setRecipients(alertOutbox.getRecipients());
		alertError.setRoles(alertOutbox.getRoles());
		return alertError;
	}
	
	public static AlertOutbox getOutboxFromError(AlertError alertError){
		AlertOutbox alertOutbox = new AlertOutbox();
		alertOutbox.setAlertName(alertError.getAlertName());
		alertOutbox.setAlertTrigger(alertError.getAlertTrigger());
		alertOutbox.setPatient(alertError.getPatient());
		alertOutbox.setMessage(alertError.getMessage());
		alertOutbox.setSubject(alertError.getSubject());
		alertOutbox.setRecipients(alertError.getRecipients());
		alertOutbox.setRoles(alertError.getRoles());
		alertOutbox.setAttempt(0);
		return alertOutbox;
	}

	public static String createEmailBody(String message, Patient patient) {
		AdministrationService as = Context.getAdministrationService();
		ObsService obs = Context.getObsService();
		ConceptService cs = Context.getConceptService();
		List<GlobalProperty> props = as.getGlobalPropertiesByPrefix("programmablealerts.concept");
		String name=patient.getPersonName().getFullName();
		String identifier=patient.getPatientIdentifier().getIdentifier();
		String baseUrl = as.getGlobalProperty("programmablealerts.base_url", "");
		message = message.replaceAll(ProgrammableAlertsConstants.BASE_URL, baseUrl);
		message = message.replaceAll(ProgrammableAlertsConstants.NAME, name);
		message = message.replaceAll(ProgrammableAlertsConstants.IDENTIFIER, identifier);
		message = message.replaceAll(ProgrammableAlertsConstants.PATIENT_ID, "" + patient.getPatientId());
		//PersonAttribute healthCenterAtt = patient.getAttribute("Health Center");
		PersonAttribute healthCenterAtt = patient.getAttribute(ProgrammableAlertsConstants.CENTRO_ATT);
		
		// Health center
		// the attribute_type name -- currently it's 'Centro'
		if (healthCenterAtt != null){
			message = message.replaceAll(ProgrammableAlertsConstants.HEALTH_CENTER_NAME, healthCenterAtt.toString());
		}
		
		//replace patient attribute
		List<PersonAttributeType> ptypes = Context.getPersonService().getAllPersonAttributeTypes();
		for (PersonAttributeType type : ptypes){
			PersonAttribute pa = patient.getAttribute(type);
			String value = "";
			if (pa != null && !pa.isVoided()) value = pa.getValue();
			message = message.replaceAll("\\{" + type.getName() + "\\}", value);
		}
		for (GlobalProperty prop : props) {
			//log.warn("prop: " +prop);
			if (message.contains("{" + prop.getDescription() + "}")){
				//there is replaceable property
				Integer conceptId;
				try{
					//log.warn("getPropValue: " +prop.getPropertyValue());
					conceptId = Integer.parseInt(prop.getPropertyValue());
				}catch (NumberFormatException e) {
					conceptId=null;
					log.error("numberformat exception in setting conceptId");
				}
				//log.warn("concept: " +concept.toString());
				if (conceptId != null){
					Concept concept = cs.getConcept(conceptId);
					//log.warn("in concept not null");
					List<Obs> observations = obs.getObservationsByPersonAndConcept(patient, concept);
					Date latestDate = null;
					String obsValue = null;
					for (Obs observation : observations) {
						//log.warn("in observations: " +observation);
						if (latestDate == null) {
							latestDate = observation.getObsDatetime();
							//log.warn("latestDate: " +latestDate);
							obsValue=observation.getValueAsString(Context.getLocale());
							//log.warn("obsValue:" +obsValue);
						} else {
							if (latestDate.before(observation.getObsDatetime())){
								latestDate=observation.getObsDatetime();
								//log.warn("latestDate2: " +latestDate);
								obsValue = observation.getValueAsString(Context.getLocale());
								//log.warn("obsValue2:" +obsValue);
							}
						}
					}
					if (obsValue != null && obsValue.trim() != ""){
						message = message.replaceAll("\\{" + prop.getDescription() + "\\}", obsValue);
						//log.warn("message:" +message);
					}
				}
			}
		}
		
		return message;
	}
}
