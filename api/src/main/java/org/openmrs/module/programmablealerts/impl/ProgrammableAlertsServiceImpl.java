package org.openmrs.module.programmablealerts.impl;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Patient;
import org.openmrs.Role;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.module.programmablealerts.AlertArchive;
import org.openmrs.module.programmablealerts.AlertError;
import org.openmrs.module.programmablealerts.AlertOutbox;
import org.openmrs.module.programmablealerts.AlertQueue;
import org.openmrs.module.programmablealerts.AlertTemplate;
import org.openmrs.module.programmablealerts.AlertTrigger;
import org.openmrs.module.programmablealerts.ProgrammableAlertsService;
import org.openmrs.module.programmablealerts.db.ProgrammableAlertsDAO;
import org.openmrs.module.reporting.cohort.definition.CohortDefinition;
import org.openmrs.module.reporting.cohort.definition.service.CohortDefinitionService;

/**
 * @author Samuel Mbugua
 */
public class ProgrammableAlertsServiceImpl implements ProgrammableAlertsService {
	@SuppressWarnings("unused")
	private static Log log = LogFactory.getLog(ProgrammableAlertsServiceImpl.class);
	
	private ProgrammableAlertsDAO dao;
	
	public ProgrammableAlertsServiceImpl() {
	}
	
	@SuppressWarnings("unused")
	private ProgrammableAlertsDAO getProgrammableAlertsDAO() {
		return dao;
	}
	
	public void setProgrammableAlertsDAO(ProgrammableAlertsDAO dao) {
		this.dao = dao;
	}
	
	public List<AlertTemplate> getAllAlertTemplates() {
		return dao.getAllAlertTemplates();
	}

	public void saveTemplate(AlertTemplate alertTemplate) {
		dao.saveTemplate(alertTemplate);		
	}

	public void deleteTemplate(AlertTemplate alertTemplate) {
		dao.deleteTemplate(alertTemplate);
	}

	public AlertTemplate getAlertTemplate(Integer templateId) {
		return dao.getAlertTemplate(templateId);
	}

	public AlertTemplate getAlertTemplateByName(String templateName) {
		return dao.getAlertTemplateByName(templateName);
	}

	public List<AlertQueue> getAllAlertsInQueue() {
		return dao.getAllAlertsInQueue();
	}

	public AlertQueue getAlert(Integer alertId) {
		return dao.getAlert(alertId);
	}

	public void deleteAlertQueue(AlertQueue alertQueue) {
		dao.deleteAlertQueue(alertQueue);
		
	}

	public AlertQueue getAlertByName(String alertName) {
		return dao.getAlertByName(alertName);
	}

	public void saveAlert(AlertQueue alertQueue) {
		dao.saveAlert(alertQueue);
	}

	public AlertTrigger getTriggerByField(String field, Integer value) {
		return dao.getTriggerByField(field, value);
	}

	public void saveTrigger(AlertTrigger alertTrigger) {
		dao.saveTrigger(alertTrigger);
	}

	public List<AlertArchive> getAllAlertArchive(Integer start, Integer length) {
		return dao.getAllAlertArchive(start, length);
	}

	public AlertArchive getAlertArchive(Integer alertId) {
		return dao.getAlertArchive(alertId);
	}

	public List<AlertError> getAllAlertErrors(Integer start, Integer length) {
		return dao.getAllAlertErrors(start, length);
	}

	public AlertError getAlertError(Integer alertId) {
		return dao.getAlertError(alertId);
	}

	public void saveOutbox(AlertOutbox outbox) {
		dao.saveOutbox(outbox);
	}

	public List<AlertOutbox> getAllAlertsInOutbox() {
		return dao.getAllAlertsInOutbox();
	}

	public void saveArchive(AlertArchive alertArchive) {
		dao.saveArchive(alertArchive);
	}

	public void saveError(AlertError alertError) {
		dao.saveError(alertError);
	}

	public List<AlertError> getErrorByPatient(String alertName, Patient patient) {
		return dao.getErrorByPatient(alertName, patient);
	}
	
	public List<AlertArchive> getArchiveByPatient(String alertName, Patient patient) {
		return dao.getArchiveByPatient(alertName, patient);
	}
	
	public List<AlertOutbox> getOutboxByPatient(String alertName, Patient patient) {
		return dao.getOutboxByPatient(alertName, patient);
	}

	public void deleteOutbox(AlertOutbox alertOutbox) {
		dao.deleteOutbox(alertOutbox);
	}

	public boolean deleteError(AlertError alertError) {
		return dao.deleteError(alertError);
	}
	public List<CohortDefinition> getAllCohortDefinition(){
		return Context.getService(CohortDefinitionService.class).getAllDefinitions(false);	
	}
        
        public List<Role> getRoles(String nameFragment, Integer start, Integer length)throws APIException{
                return dao.getRoles(nameFragment, start, length);
        }
        
        public Role getRole(Integer roleId) throws APIException{
                return dao.getRole(roleId);
        }

        public List<Role> getAllRoles() throws APIException{
                return dao.getAllRoles();
        }
}