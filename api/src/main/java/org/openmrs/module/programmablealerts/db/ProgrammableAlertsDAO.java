package org.openmrs.module.programmablealerts.db;

import java.util.List;

import org.openmrs.Patient;
import org.openmrs.Role;
import org.openmrs.api.db.DAOException;
import org.openmrs.module.programmablealerts.AlertArchive;
import org.openmrs.module.programmablealerts.AlertError;
import org.openmrs.module.programmablealerts.AlertOutbox;
import org.openmrs.module.programmablealerts.AlertQueue;
import org.openmrs.module.programmablealerts.AlertTemplate;
import org.openmrs.module.programmablealerts.AlertTrigger;


/**
 * Public Interface to the {@link}HibernateProgrammableAlertsDAO
 * 
 * @author Samuel Mbugua
 */
public interface ProgrammableAlertsDAO {

	public List<AlertTemplate> getAllAlertTemplates();

	public void saveTemplate(AlertTemplate alertTemplate);

	public void deleteTemplate(AlertTemplate alertTemplate);

	public AlertTemplate getAlertTemplate(Integer templateId);

	public AlertTemplate getAlertTemplateByName(String templateName);

	public List<AlertQueue> getAllAlertsInQueue();

	public AlertQueue getAlert(Integer alertId);

	public void deleteAlertQueue(AlertQueue alertQueue);

	public AlertQueue getAlertByName(String alertName);

	public void saveAlert(AlertQueue alertQueue);

	public AlertTrigger getTriggerByField(String field, Integer value);

	public void saveTrigger(AlertTrigger alertTrigger);

	public List<AlertArchive> getAllAlertArchive(Integer start, Integer length);

	public AlertArchive getAlertArchive(Integer alertId);

	public List<AlertError> getAllAlertErrors(Integer start, Integer length);

	public AlertError getAlertError(Integer alertId);

	public void saveOutbox(AlertOutbox outbox);

	public List<AlertOutbox> getAllAlertsInOutbox();

	public void saveArchive(AlertArchive alertArchive);

	public void saveError(AlertError alertError);

	public List<AlertError> getErrorByPatient(String alertName, Patient patient);
	
	public List<AlertArchive> getArchiveByPatient(String alertName, Patient patient);
	
	public List<AlertOutbox> getOutboxByPatient(String alertName, Patient patient);

	public void deleteOutbox(AlertOutbox alertOutbox);

	public boolean deleteError(AlertError alertError);
        
        public List<Role> getRoles(String nameFragment, Integer start, Integer length) throws DAOException;
        
        public Role getRole(Integer roleId) throws DAOException;
        
        public List<Role> getAllRoles() throws DAOException;
        
}