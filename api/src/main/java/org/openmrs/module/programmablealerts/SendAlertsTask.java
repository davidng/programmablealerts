package org.openmrs.module.programmablealerts;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.scheduler.tasks.AbstractTask;

/**
 * A task to automatically send emails.
 * 
 * @author Samuel Mbugua
 */
public class SendAlertsTask  extends AbstractTask {

	private static Log log = LogFactory.getLog(SendAlertsTask.class);

	// An instance of the ProgrammableAlertsProcessor processor.
	private SendAlertsProcess processor = null;

	/**
	 * Default Convenience constructor 
	 */
	public SendAlertsTask() {
		if (processor == null)
			processor = new SendAlertsProcess();
	}

	/**
	 * No comment for now
	 */
	public void execute() {
		Context.openSession();
		log.debug("Running Send Alerts Task... ");
		try {
			if (Context.isAuthenticated() == false)
				authenticate();
			processor.startAlertsSender();
		} catch (APIException e) {
			log.error("Error running Send Alerts Task", e);
			throw e;
		} finally {
			Context.closeSession();
		}
	}

	/*
	 * Resources clean up
	 */
	public void shutdown() {
		processor = null;
		super.shutdown();
		log.debug("Shutting down Send Alerts task ...");
	}
}