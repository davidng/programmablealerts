package org.openmrs.module.programmablealerts;

import java.util.List;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.User;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.module.programmablealerts.util.ProgrammableAlertsConstants;
import org.openmrs.module.programmablealerts.util.EmailGateway;
import org.openmrs.module.programmablealerts.util.ProgrammableAlertsUtils;
import org.openmrs.util.OpenmrsConstants;
import org.springframework.transaction.annotation.Transactional;


/**
 * Processes automated alerts
 * 
 * @author Samuel Mbugua
 *
 */
@Transactional
public class SendAlertsProcess {

	private static final Log log = LogFactory.getLog(SendAlertsProcess.class);
	private ProgrammableAlertsService alertsService;
	private EmailGateway gateway;
	
	// allow only one running instance
	private static Boolean isRunning = false; 

	public SendAlertsProcess(){
		try{
			this.getAlertsService();
		}
		catch(Exception e){
		}
	}

	private void sendAlerts() {
		gateway = new EmailGateway();
		gateway.startup();
		if (gateway.canSend()){
			log.info("Its posible to send mails:");
			String subject, strMessage;
			String[] toAddress;
			List<AlertOutbox> alerts = alertsService.getAllAlertsInOutbox();
			for (AlertOutbox alert : alerts) {
				subject = alert.getSubject();
				strMessage = alert.getMessage();
				String recipients="";
				List<User> users = ProgrammableAlertsUtils.getRecipients(alert.getRecipients());
				List<User> users2 = ProgrammableAlertsUtils.getRecipientsByRoles(alert.getRoles());
				ArrayList<Integer> userIds = new ArrayList<Integer>();
				for(User user : users) userIds.add(user.getId());
				for(User user : users2){
					if(!userIds.contains(user.getId())){
						users.add(user);
						userIds.add(user.getId());
					}
				}				

				for (User user : users) {
					recipients=recipients + user.getUserProperty(
							OpenmrsConstants.USER_PROPERTY_NOTIFICATION_ADDRESS) + ",";
				}
				recipients = recipients.substring(0,recipients.lastIndexOf(","));
				toAddress=recipients.split(","); 
				if (gateway.sendMessage(subject, strMessage, toAddress)){
					//move message to archive and delete from outbox
					alertsService.saveArchive(ProgrammableAlertsUtils.getArchiveFromOutbox(alert));
					alertsService.deleteOutbox(alert);
				}else{
					Integer attempt = ProgrammableAlertsUtils.getSendAttempts(
							Context.getAdministrationService().getGlobalProperty(ProgrammableAlertsConstants.GP_SEND_ATTEMPTS));
					
					if (alert.getAttempt() < attempt){
						alert.setAttempt(alert.getAttempt() + 1);
						alertsService.saveOutbox(alert);
					}else{
						log.error("there was error sending email");
						//move to error with relevant message and remove from queue
						alertsService.saveError(ProgrammableAlertsUtils.getErrorFromOutbox(alert));
						alertsService.deleteOutbox(alert);
					}
				}
			}
		} else {
			log.debug("outgoing server session is not turned on");
		}
		gateway.shutdown();
	}

	public void startAlertsSender() {
		synchronized (isRunning) {
			if (isRunning) {
				log.warn("SendAlerts processor aborting (another processor already running)");
				return;
			}
			isRunning = true;
		}

		try {	
			sendAlerts();
		}
		catch(Exception e){
			log.error("Problem occured while Sending automated alerts", e);
		}
		finally {
			isRunning = false;
		}
	}

	
	/**
	 * @return ProgrammableAlertsService to be used by the process
	 */
	private ProgrammableAlertsService getAlertsService() {
		if (alertsService == null) {
			try {
				alertsService= (ProgrammableAlertsService)Context.getService(ProgrammableAlertsService.class);
			}catch (APIException e) {
				log.debug("ProgrammableAlertsService not found");
				return null;
			}
		}
		return alertsService;
	}
	
}
