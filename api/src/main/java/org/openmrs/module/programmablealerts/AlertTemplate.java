package org.openmrs.module.programmablealerts;

import java.util.Date;

import org.openmrs.Auditable;
import org.openmrs.BaseOpenmrsObject;
import org.openmrs.User;

/**
 * POJO for the AlertTemplate table, the templates are the
 * basis for forming email messages
 * @author Samuel Mbugua
 */
public class AlertTemplate extends BaseOpenmrsObject implements Auditable{
	private Integer templateId;
	private String templateName;
	private String subject;
	private String message;
	private User creator;
	private Date dateCreated;
	/**
	 * @return the templateId
	 */
	public Integer getTemplateId() {
		return templateId;
	}
	/**
	 * @param templateId the templateId to set
	 */
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}
	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}
	/**
	 * @param templateName the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the creator
	 */
	public User getCreator() {
		return creator;
	}
	/**
	 * @param creator the creator to set
	 */
	public void setCreator(User creator) {
		this.creator = creator;
	}
	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}
	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Integer getId() {
		return getTemplateId();
	}
	public void setId(Integer id) {
		this.setTemplateId(id);
		
	}
	public User getChangedBy() {
		// TODO Auto-generated method stub
		return null;
	}
	public Date getDateChanged() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setChangedBy(User arg0) {
		// TODO Auto-generated method stub
		
	}
	public void setDateChanged(Date arg0) {
		// TODO Auto-generated method stub
		
	}
}
