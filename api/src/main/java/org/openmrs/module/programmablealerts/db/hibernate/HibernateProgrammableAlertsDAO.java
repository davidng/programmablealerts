package org.openmrs.module.programmablealerts.db.hibernate;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.openmrs.Patient;
import org.openmrs.Role;
import org.openmrs.api.db.DAOException;
import org.openmrs.module.programmablealerts.AlertArchive;
import org.openmrs.module.programmablealerts.AlertError;
import org.openmrs.module.programmablealerts.AlertOutbox;
import org.openmrs.module.programmablealerts.AlertQueue;
import org.openmrs.module.programmablealerts.AlertTemplate;
import org.openmrs.module.programmablealerts.AlertTrigger;
import org.openmrs.module.programmablealerts.db.ProgrammableAlertsDAO;

/**
 * Database interface for the module 
 * 
 * @author Samuel Mbugua
 *
 */
public class HibernateProgrammableAlertsDAO implements ProgrammableAlertsDAO {

	/**
	 * Hibernate session factory
	 */
	private SessionFactory sessionFactory;
	
	/**
	 * Default public constructor
	 */
	public HibernateProgrammableAlertsDAO() { }
	
	/**
	 * Set session factory
	 * 
	 * @param sessionFactory
	 */
	public void setSessionFactory(SessionFactory sessionFactory) { 
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	public List<AlertTemplate> getAllAlertTemplates() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertTemplate.class);
		return (List<AlertTemplate>)criteria.list();
	}

	public void saveTemplate(AlertTemplate alertTemplate) {
		sessionFactory.getCurrentSession().saveOrUpdate(alertTemplate);		
	}

	public void deleteTemplate(AlertTemplate alertTemplate) {
		sessionFactory.getCurrentSession().delete(alertTemplate);
	}

	public AlertTemplate getAlertTemplate(Integer templateId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertTemplate.class);
		criteria.add(Expression.like("templateId", templateId));
		return (AlertTemplate) criteria.uniqueResult();
	}

	public AlertTemplate getAlertTemplateByName(String templateName) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertTemplate.class);
		criteria.add(Expression.like("templateName", templateName));
		return (AlertTemplate) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<AlertQueue> getAllAlertsInQueue() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertQueue.class);
		return (List<AlertQueue>)criteria.list();
	}

	public AlertQueue getAlert(Integer alertId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertQueue.class);
		criteria.add(Expression.like("id", alertId));
		return (AlertQueue) criteria.uniqueResult();
	}

	public void deleteAlertQueue(AlertQueue alertQueue) {
		sessionFactory.getCurrentSession().delete(alertQueue);
	}

	public AlertQueue getAlertByName(String alertName) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertQueue.class);
		criteria.add(Expression.like("alertName", alertName));
		return (AlertQueue) criteria.uniqueResult();
	}

	public void saveAlert(AlertQueue alertQueue) {
		sessionFactory.getCurrentSession().saveOrUpdate(alertQueue);
	}

	public AlertTrigger getTriggerByField(String field, Integer value) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertTrigger.class);
		criteria.add(Expression.like(field, value));
		return (AlertTrigger) criteria.uniqueResult();
		
	}

	public void saveTrigger(AlertTrigger alertTrigger) {
		sessionFactory.getCurrentSession().saveOrUpdate(alertTrigger);
	}

	@SuppressWarnings("unchecked")
	public List<AlertArchive> getAllAlertArchive(Integer start, Integer length) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertArchive.class);
		if (start != null)
			criteria.setFirstResult(start);
		if (length != null && length > 0)
			criteria.setMaxResults(length);
		criteria.addOrder(Order.desc("dateSent"));
		return (List<AlertArchive>)criteria.list();
	}

	public AlertArchive getAlertArchive(Integer alertId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertArchive.class);
		criteria.add(Expression.like("id", alertId));
		return (AlertArchive) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<AlertError> getAllAlertErrors(Integer start, Integer length) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertError.class);
		if (start != null)
			criteria.setFirstResult(start);
		if (length != null && length > 0)
			criteria.setMaxResults(length);
		criteria.addOrder(Order.desc("lastAttempt"));
		return (List<AlertError>)criteria.list();
	}

	public AlertError getAlertError(Integer alertId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertError.class);
		criteria.add(Expression.like("id", alertId));
		return (AlertError) criteria.uniqueResult();	}

	public void saveOutbox(AlertOutbox outbox) {
		sessionFactory.getCurrentSession().saveOrUpdate(outbox);
	}

	@SuppressWarnings("unchecked")
	public List<AlertOutbox> getAllAlertsInOutbox() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertOutbox.class);
		return (List<AlertOutbox>)criteria.list();
	}

	public void saveArchive(AlertArchive alertArchive) {
		sessionFactory.getCurrentSession().saveOrUpdate(alertArchive);
	}

	public void saveError(AlertError alertError) {
		sessionFactory.getCurrentSession().saveOrUpdate(alertError);
	}

	@SuppressWarnings("unchecked")
	public List<AlertError> getErrorByPatient(String alertName, Patient patient) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertError.class);
		criteria.add(Expression.like("alertName", alertName));
		criteria.add(Expression.like("patient", patient));
		return (List<AlertError>)criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<AlertArchive> getArchiveByPatient(String alertName, Patient patient) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertArchive.class);
		criteria.add(Expression.like("alertName", alertName));
		criteria.add(Expression.like("patient", patient));
		return (List<AlertArchive>)criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<AlertOutbox> getOutboxByPatient(String alertName, Patient patient) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(AlertOutbox.class);
		criteria.add(Expression.like("alertName", alertName));
		criteria.add(Expression.like("patient", patient));
		return (List<AlertOutbox>)criteria.list();
	}

	public void deleteOutbox(AlertOutbox alertOutbox) {
		sessionFactory.getCurrentSession().delete(alertOutbox);
	}

	public boolean deleteError(AlertError alertError) {
		sessionFactory.getCurrentSession().delete(alertError);
		return true;
	}
        
        
        @SuppressWarnings("unchecked")
	@Override
	public List<Role> getRoles(String nameFragment, Integer start, Integer length)
	        throws DAOException {
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Role.class);
		
		if (StringUtils.isNotBlank(nameFragment))
			criteria.add(Restrictions.ilike("role", nameFragment, MatchMode.START));
		
		criteria.addOrder(Order.asc("role"));
		if (start != null)
			criteria.setFirstResult(start);
		if (length != null && length > 0)
			criteria.setMaxResults(length);
		
		return criteria.list();
	}      
        
        public Role getRole(Integer roleId) {
		return (Role) sessionFactory.getCurrentSession().get(Role.class, roleId);
	}
        
        public List<Role> getAllRoles(){
                Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Role.class);		
                criteria.addOrder(Order.asc("role"));
		return criteria.list();
        }
}