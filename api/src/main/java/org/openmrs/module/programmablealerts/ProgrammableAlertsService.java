package org.openmrs.module.programmablealerts;

import java.util.List;
import org.openmrs.Patient;
import org.openmrs.Role;
import org.openmrs.annotation.Authorized;
import org.openmrs.api.APIException;
import org.openmrs.module.reporting.cohort.definition.CohortDefinition;
import org.openmrs.util.PrivilegeConstants;
import org.springframework.transaction.annotation.Transactional;

/**
 * Required methods for programmable alerts service
 * 
 * @author  Samuel Mbugua
 */
@Transactional
public interface ProgrammableAlertsService {

	/**
	 * Return all alert templates
	 * @return {List<AlertTemplate>}
	 */
	public List<AlertTemplate> getAllAlertTemplates();
	
	/**
	 * 

	 * @param alertTemplate
	 */
	public void saveTemplate(AlertTemplate alertTemplate);

	/**
	 * Delete specified template
	 * @param templateId
	 */
	public void deleteTemplate(AlertTemplate alertTemplate);
	
	/**
	 * Return an alert template given its id
	 * @param templateId
	 * @return {@link AlertTemplate}
	 */
	public AlertTemplate getAlertTemplate(Integer templateId);

	/**
	 * Return an alert given its absolute name
	 * @param templateName
	 * @return {@link AlertTemplate}
	 */
	public AlertTemplate getAlertTemplateByName(String templateName);

	/**
	 * Return all Alerts in the queue i.e. Alerts pending to be sent out
	 * @return a list of {@link AlertQueue}(s)
	 */
	public List<AlertQueue> getAllAlertsInQueue();

	/**
	 * Return an alert given its id
	 * @param alertId
	 * @return {@link AlertQueue}
	 */
	public AlertQueue getAlert(Integer alertId);

	/**
	 * Delete specified alertQueue 
	 * @param alertQueue
	 */
	public void deleteAlertQueue(AlertQueue alertQueue);

	/**
	 * Return an AlertQueue given its name
	 * @param alertName
	 * @return {@link AlertQueue}
	 */
	public AlertQueue getAlertByName(String alertName);

	/**
	 * Save or update a single {@link AlertQueue}
	 * @param alertQueue
	 */
	public void saveAlert(AlertQueue alertQueue);

	/**
	 * Return all {@link AlertTrigger} with field having value
	 * @param field
	 * @param value
	 * @return
	 */
	public AlertTrigger getTriggerByField(String field, Integer value);

	/**
	 * Save or update a single {@link AlertTrigger}
	 * @param alertTrigger
	 */
	public void saveTrigger(AlertTrigger alertTrigger);

	/**
	 * Return all sent alerts
	 * @return a list of {@link AlertArchive}
	 */
	public List<AlertArchive> getAllAlertArchive(Integer start, Integer length);

	/**
	 * Return an {@link AlertArchive} given its id
	 * @param parseInt
	 * @return {@link AlertArchive}
	 */
	public AlertArchive getAlertArchive(Integer alertId);

	/**
	 * Return all alert errors
	 * @param length 
	 * @param start 
	 * @return a list of {@link AlertError}
	 */
	public List<AlertError> getAllAlertErrors(Integer start, Integer length);

	/**
	 * Return an {@link AlertError} given its id
	 * @param alertId
	 * @return {@link AlertError}
	 */
	public AlertError getAlertError(Integer alertId);

	/**Save or update a single {@link AlertOutbox}
	 * @param outbox
	 */
	public void saveOutbox(AlertOutbox outbox);


	/**
	 * Return all alerts outbox messages
	 * @return {@link AlertOutbox}}
	 */
	public List<AlertOutbox> getAllAlertsInOutbox();


	/**
	 * Save or Update a single {@link AlertArchive}
	 * @param alertArchive
	 */
	public void saveArchive(AlertArchive alertArchive);


	/**
	 * Save or Update a single {@link AlertError}
	 * @param alertError
	 */
	public void saveError(AlertError alertError);

	/** Return an error with same name and for same patient
	 * @param alertName
	 * @param patientId
	 * @return {@link AlertError}
	 */
	public List<AlertError> getErrorByPatient(String alertName, Patient patient);
	
	/**
	 * Return an archive with same name and for same patient
	 * @param alertName
	 * @param patient
	 * @return list {@link AlertArchive}
	 */
	public List<AlertArchive> getArchiveByPatient(String alertName, Patient patient);
	
	/**
	 * Return an outbox with same name and for same patient
	 * @param alertName
	 * @param patient
	 * @return {@link AlertOutbox}
	 */
	public List<AlertOutbox> getOutboxByPatient(String alertName, Patient patient);

	/**
	 * Delete outbox message
	 * @param alert
	 */
	public void deleteOutbox(AlertOutbox alertOutbox);

	/**
	 * Delete an error from the database
	 * @param alertError
	 * @return
	 */
	public boolean deleteError(AlertError alertError);
	
	/**
	 * return all cohort definition
	 * @return
	 */
	public List<CohortDefinition> getAllCohortDefinition();
        
        
        @Transactional(readOnly = true)
	@Authorized( { PrivilegeConstants.VIEW_ROLES })
	public List<Role> getRoles(String nameFragment, Integer start, Integer length)
	        throws APIException;
        
        @Transactional(readOnly = true)
	@Authorized( { PrivilegeConstants.VIEW_ROLES })
	public Role getRole(Integer roleId) throws APIException;  
 
        @Transactional(readOnly = true)
	@Authorized( { PrivilegeConstants.VIEW_ROLES })
	public List<Role> getAllRoles() throws APIException;
}