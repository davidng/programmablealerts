package org.openmrs.module.programmablealerts;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.module.Activator;

/**
 * Activator startup/shutdown methods for the Alerts module
 *
 * @author  Samuel Mbugua
 */
public class ProgrammableAlertsActivator implements Activator {

	private Log log = LogFactory.getLog(this.getClass());
	
	/**
	 * @see org.openmrs.module.Activator#startup()
	 */
	public void startup() {
		log.info("Starting the Programmable Alerts module");
	}
	
	/**
	 *  @see org.openmrs.module.Activator#shutdown()
	 */
	public void shutdown() {
		log.info("Shutting down the Programmabl Alerts module");
	}
	
}
