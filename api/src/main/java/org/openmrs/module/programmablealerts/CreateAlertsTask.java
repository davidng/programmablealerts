package org.openmrs.module.programmablealerts;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.scheduler.tasks.AbstractTask;

/**
 * A task to automatically create emails from saved searches
 * and specified templates.
 * 
 * @author Samuel Mbugua
 */
public class CreateAlertsTask  extends AbstractTask {

	private static Log log = LogFactory.getLog(CreateAlertsTask.class);

	// An instance of the ProgrammableAlertsProcessor processor.
	private CreateAlertsProcess processor = null;

	/**
	 * Default Convenience constructor 
	 */
	public CreateAlertsTask() {
		if (processor == null)
			processor = new CreateAlertsProcess();
	}

	/**
	 * No comment for now
	 */
	public void execute() {
		Context.openSession();
		log.warn("Running Create Alerts Task");
		try {
			if (Context.isAuthenticated() == false)
				authenticate();
			processor.startCreateAlerts();
		} catch (APIException e) {
			log.error("Error running Create Alerts Task", e);
			throw e;
		} finally {
			Context.closeSession();
		}
	}

	/*
	 * Resources clean up
	 */
	public void shutdown() {
		processor = null;
		super.shutdown();
		log.warn("Shutting down Create Alerts task");
	}
}