package org.openmrs.module.programmablealerts;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.User;
import org.openmrs.api.context.Context;
import org.openmrs.util.OpenmrsConstants;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class CopyUsernameToNotificationProcess
{
	private static Log log = LogFactory.getLog(CopyUsernameToNotificationProcess.class);
	private static Boolean isRunning = false;
	
	public CopyUsernameToNotificationProcess(){
		
	}
	
	private void copyUsername(User user){
		String username = user.getUsername();
		if (username.indexOf("@") > 0){
			String notificationAddress = user.getUserProperty(OpenmrsConstants.USER_PROPERTY_NOTIFICATION_ADDRESS);
			if (!StringUtils.equals(username, notificationAddress)){
				user.setUserProperty("notificationAddressOld", notificationAddress);
				user.setUserProperty(OpenmrsConstants.USER_PROPERTY_NOTIFICATION_ADDRESS, username);
			}
		}
	}
	
	private void runCopyUsername(){
		List<User> users = Context.getUserService().getAllUsers();
		for(User u : users){
			copyUsername(u);
		}
	}

	public void startCopyUsername() {
		synchronized (isRunning) {
			if (isRunning) {
				log.warn("SendAlerts processor aborting (another processor already running)");
				return;
			}
			isRunning = true;
		}

		try {	
			runCopyUsername();
		}
		catch(Exception e){
			log.error("Problem occured while copying username", e);
		}
		finally {
			isRunning = false;
		}
	}

}
