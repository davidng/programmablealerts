package org.openmrs.module.programmablealerts;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.scheduler.tasks.AbstractTask;

public class CopyUsernameToNotificationTask extends AbstractTask{

	private static Log log = LogFactory.getLog(CopyUsernameToNotificationTask.class);
	private CopyUsernameToNotificationProcess processor = null;
	
	public CopyUsernameToNotificationTask() {
		if (processor == null)
			processor = new CopyUsernameToNotificationProcess();
	}
	@Override
	public void execute() {
		Context.openSession();
		log.debug("Running Copy Username to Notification Task... ");
		try {
			if (Context.isAuthenticated() == false)
				authenticate();
			processor.startCopyUsername();
		} catch (APIException e) {
			log.error("Error running Copy Username to Notification Task", e);
			throw e;
		} finally {
			Context.closeSession();
		}
		
	}
	
	/*
	 * Resources clean up
	 */
	public void shutdown() {
		processor = null;
		super.shutdown();
		log.debug("Shutting down Copy Username task ...");
	}

}
