package org.openmrs.module.programmablealerts;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Patient;
import org.openmrs.api.APIException;
import org.openmrs.api.context.Context;
import org.openmrs.module.programmablealerts.util.ProgrammableAlertsConstants;
import org.openmrs.module.programmablealerts.util.ProgrammableAlertsUtils;
import org.springframework.transaction.annotation.Transactional;


/**
 * Create automated alerts (emails) by affixing actual person details
 * to email templates and pushing the created alerts into the outbox
 * ready for dispatching.
 * 
 * @author Samuel Mbugua
 *
 */
@Transactional
public class CreateAlertsProcess {

	private static final Log log = LogFactory.getLog(CreateAlertsProcess.class);
	private ProgrammableAlertsService alertsService;
	
	// allow only one running instance
	private static Boolean isRunning = false; 

	public CreateAlertsProcess(){
		this.getAlertsService();
	}

	private void createAlerts() {
		//first ensure there are templates
		log.warn("Creating Alerts");
		List<AlertQueue> alertQueues = alertsService.getAllAlertsInQueue();
		String repeatAlerts = Context.getAdministrationService().getGlobalProperty(ProgrammableAlertsConstants.GP_REPEAT_ALERTS);
		if (alertQueues !=null && alertQueues.size() >= 1){
			//process every AlertQueue
			for (AlertQueue alertQueue : alertQueues) {
				//create a message for every patient in savedSearch 
				Integer savedSearchId= alertQueue.getAlertTrigger().getSavedSearch();
				//List<Patient> patients = ProgrammableAlertsUtils.getSavedSearchPatientSet(savedSearchId);
				List<Patient> patients = ProgrammableAlertsUtils.getSavedSearchPatientSetFromCohortDefinition(savedSearchId);
				for (Patient patient : patients) {
					if (repeatAlerts.equalsIgnoreCase("false") && sameAlertForPatient(alertQueue.getAlertName(), patient)) 
						continue;
					try {
						//create and save outbox message
						alertsService.saveOutbox(ProgrammableAlertsUtils.getOutboxFromQueue(alertQueue, patient));
					} catch (Exception e) {
						//an uncaptured error
						e.printStackTrace();
					}
				}
			}
		}
	}

	public void startCreateAlerts() {
		synchronized (isRunning){
			if (isRunning) {
				log.warn("Create alerts processor aborting (another processor already running)");
				return;
			}
			isRunning = true;
		}

		try {	
			createAlerts();
		}
		catch(Exception e){
			log.error("Problem occured while creating automated alerts", e);
		}
		finally {
			isRunning = false;
		}
	}
	
	/**
	 * Whether or not same alert is dispatched for this patient 
	 * @param alertId
	 * @param patientId
	 * @return <b>true</b> if same alert sent for this patient otherwise <b>false</b>
	 */
	private boolean sameAlertForPatient(String alertName, Patient patient) {
		boolean alertSent = false;
		
		//find in the error queue
		if (alertsService.getErrorByPatient(alertName,patient).size() > 0) 
			alertSent = true;
		
		//find in the archive (sent) queue
		if (alertsService.getArchiveByPatient(alertName,patient).size() > 0) 
			alertSent = true;
		
		//find in the outbox queue
		if (alertsService.getOutboxByPatient(alertName,patient).size() > 0) 
			alertSent = true;
		
		return alertSent;
	}

	
	/**
	 * @return ProgrammableAlertsService to be used by the process
	 */
	private ProgrammableAlertsService getAlertsService() {
		if (alertsService == null) {
			//log.warn("alertsService is null");
			try {
				alertsService= (ProgrammableAlertsService)Context.getService(ProgrammableAlertsService.class);
			}catch (APIException e) {
				log.debug("ProgrammableAlertsService not found");
				return null;
			}
		}
		//log.warn("alertsService: " +alertsService);
		return alertsService;
	}
	
}