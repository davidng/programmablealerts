package org.openmrs.module.programmablealerts;

import java.util.Date;

import org.openmrs.Auditable;
import org.openmrs.BaseOpenmrsObject;
import org.openmrs.User;

/**
 * POJO for the AlertQueue table, it is an alert in its
 * raw form pending dispatch
 * @author Samuel Mbugua
 */
public class AlertQueue extends BaseOpenmrsObject implements Auditable{
	private Integer id;
	private String alertName;
	private String recipients;
	private String roles;
	private AlertTemplate template;
	private AlertTrigger alertTrigger;
	private User creator;
	private Date dateCreated;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the alertName
	 */
	public String getAlertName() {
		return alertName;
	}
	/**
	 * @param alertName the alertName to set
	 */
	public void setAlertName(String alertName) {
		this.alertName = alertName;
	}
	/**
	 * @return the recipients
	 */
	public String getRecipients() {
		return recipients == null ? "" : recipients;
	}
	
	/**
	 * @param recipients the recipients to set
	 */
	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(String roles) {
		this.roles = roles;
	}

	/**
	 * @return the roles
	 */
	public String getRoles() {
		return roles == null ? "" : roles;
	}


	/**
	 * @return the template
	 */
	public AlertTemplate getTemplate() {
		return template;
	}
	/**
	 * @param template the template to set
	 */
	public void setTemplate(AlertTemplate template) {
		this.template = template;
	}
	/**
	 * @return the alertTrigger
	 */
	public AlertTrigger getAlertTrigger() {
		return alertTrigger;
	}
	/**
	 * @param alertTrigger the alertTrigger to set
	 */
	public void setAlertTrigger(AlertTrigger alertTrigger) {
		this.alertTrigger = alertTrigger;
	}
	/**
	 * @return the creator
	 */
	public User getCreator() {
		return creator;
	}
	/**
	 * @param creator the creator to set
	 */
	public void setCreator(User creator) {
		this.creator = creator;
	}
	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}
	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public User getChangedBy() {
		// TODO Auto-generated method stub
		return null;
	}
	public Date getDateChanged() {
		// TODO Auto-generated method stub
		return null;
	}
	public void setChangedBy(User arg0) {
		// TODO Auto-generated method stub
		
	}
	public void setDateChanged(Date arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
}
