package org.openmrs.module.programmablealerts.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.context.Context;

/**
 * A gateway for sending emails
 * 
 * @author Samuel Mbugua
 * 
 */
public class EmailGateway {
	
	private static Log log = LogFactory.getLog(ProgrammableAlertsUtils.class);
	private Session session;

	public void startup() {
		final AdministrationService adminService = Context.getAdministrationService();
		Properties props = new Properties();
		props.setProperty(ProgrammableAlertsConstants.GP_MAIL_PROTOCOL,
				adminService.getGlobalProperty(ProgrammableAlertsConstants.GP_MAIL_PROTOCOL_VALUE));
	    props.setProperty(ProgrammableAlertsConstants.GP_MAIL_STARTTLS_ENABLE, 
	    		adminService.getGlobalProperty(ProgrammableAlertsConstants.GP_MAIL_STARTTLS_ENABLE_VALUE));
	    props.setProperty(ProgrammableAlertsConstants.GP_MAIL_HOST,
	    		adminService.getGlobalProperty(ProgrammableAlertsConstants.GP_MAIL_HOST_VALUE));
	    props.setProperty(ProgrammableAlertsConstants.GP_MAIL_PORT, 
	    		adminService.getGlobalProperty(ProgrammableAlertsConstants.GP_MAIL_PORT_VALUE));
	    props.setProperty(ProgrammableAlertsConstants.GP_MAIL_AUTH,
	    		adminService.getGlobalProperty(ProgrammableAlertsConstants.GP_MAIL_AUTH_VALUE));
	    props.setProperty(ProgrammableAlertsConstants.GP_MAIL_FROM,
	    		adminService.getGlobalProperty(ProgrammableAlertsConstants.GP_MAIL_FROM_VALUE));
	    
	    Authenticator auth = new Authenticator() {

			@Override
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(
						adminService.getGlobalProperty(ProgrammableAlertsConstants.GP_MAIL_USER), 
						adminService.getGlobalProperty(ProgrammableAlertsConstants.GP_MAIL_PASSWORD));
			}
		};
		session = Session.getInstance(props, auth);
		log.warn("started gate way successfully");
	}

	public boolean sendMessage(String subject, String content, String[] recipients) {
	
		try {
		    MimeMessage message = new MimeMessage(session);
		    message.addRecipients(Message.RecipientType.TO, getToAddresses(recipients));
		    message.setSubject(subject);
		    message.setText(content);
		    Transport.send(message);
		    log.debug("message sent successfully");
		    return true;
		} catch (MessagingException e) {
			//check errors and act accordingly
			log.error("Error sending message: " + e.getMessage());
			ProgrammableAlertsConstants.errorMessage=e.getMessage();
		}
		return false;
	}

	private  static InternetAddress[] getToAddresses(String[] address) {
		InternetAddress[] toAddress = new InternetAddress[address.length];
		
	    // To get the array of addresses
	    for( int i=0; i < address.length; i++ ) {
	        try {
	        	toAddress[i] = new InternetAddress(address[i]);
			} catch (AddressException e) {
				log.error("Error converting address: " + address[i] + " the error is: " + e.getMessage());
			}
	    }
		return toAddress;
	}
	
	public boolean canSend() {
		return session != null;
	}
	
	public void shutdown() {
		log.warn("shutting down gateway");
		session = null;
	}
}
