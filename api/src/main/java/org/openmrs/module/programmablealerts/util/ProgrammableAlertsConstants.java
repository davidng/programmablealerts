package org.openmrs.module.programmablealerts.util;

/**
 * Module wide constants are kept here
 * 
 * @author Samuel Mbugua
 * 
 */
public class ProgrammableAlertsConstants {
	public static final String NAME = "\\{Person Name\\}";
	public static final String IDENTIFIER="\\{Person Identifier\\}";
	public static final String PATIENT_ID="\\{Patient Id\\}";
	public static final String HEALTH_CENTER_ATT="Centro";
	public static final String HEALTH_CENTER="\\{Centro\\}";
	public static final String HEALTH_CENTER_NAME="\\{Health Center Name\\}";
	public static final int CENTRO_ATT = 7;// the id of the person attribute whose name is Centro or Health Center
	public static final String BASE_URL="\\{Base URL\\}";
	
	
	public static String errorMessage;
	
	//global properties
	public static final String GP_MAIL_USER = "mail.user";
	public static final String GP_MAIL_PASSWORD = "mail.password";
	public static final String GP_MAIL_PROTOCOL = "mail.transport.protocol";
	public static final String GP_MAIL_HOST = "mail.smtp.host";
	public static final String GP_MAIL_PORT = "mail.smtp.port";
	public static final String GP_MAIL_AUTH = "mail.smtp.auth";
	public static final String GP_MAIL_FROM = "mail.smtp.from";
	public static final String GP_MAIL_STARTTLS_ENABLE="mail.smtp.starttls.enable";
	
	public static final String GP_REPEAT_ALERTS = "programmablealerts.repeat_alerts";
	public static final String GP_SEND_ATTEMPTS = "programmablealerts.send_attempts";
	
	//global properties values
	public static final String GP_MAIL_PROTOCOL_VALUE = "mail.transport_protocol";
	public static final String GP_MAIL_HOST_VALUE = "mail.smtp_host";
	public static final String GP_MAIL_PORT_VALUE = "mail.smtp_port";
	public static final String GP_MAIL_AUTH_VALUE = "mail.smtp_auth";
	public static final String GP_MAIL_FROM_VALUE = "mail.from";
	public static final String GP_MAIL_STARTTLS_ENABLE_VALUE="mail.smtp_starttls_enable";
	
	//PRIVILEGES
	public static final String PRIV_RESOLVE_MOBILE_FORM_ENTRY_ERROR = "Resolve Mobile Form Entry Error";
	public static final String PRIV_COMMENT_ON_MOBILE_FORM_ENTRY_ERRORS = "Comment on Mobile Form Entry Errors";
	public static final String PRIV_VIEW_MOBILE_FORM_ERROR = "View Mobile Form Error";
	public static final String PRIV_VIEW_MOBILE_FORM_PROPERTY = "View Mobile Form Entry Properties";
	public static final String PRIV_MANAGE_ECONOMIC_OBJECT = "Manage Economic Objects";
}
